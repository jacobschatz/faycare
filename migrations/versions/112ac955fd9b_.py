"""empty message

Revision ID: 112ac955fd9b
Revises: 22a9974b885b
Create Date: 2014-04-17 21:33:44.071525

"""

# revision identifiers, used by Alembic.
revision = '112ac955fd9b'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('age_range',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('school_id', sa.Integer(), nullable=True),
    sa.Column('kid_id', sa.Integer(), nullable=True),
    sa.Column('start_age', sa.Integer(), nullable=True),
    sa.Column('end_age', sa.Integer(), nullable=True),
    sa.Column('needs_potty_trained', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['kid_id'], ['kid.id'], ),
    sa.ForeignKeyConstraint(['school_id'], ['school.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column(u'price', sa.Column('age_range_id', sa.Integer(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column(u'price', 'age_range_id')
    op.drop_table('age_range')
    ### end Alembic commands ###
