$(function(){
	$("div.uk-button-group button").on('click',function(e){
		$("div.uk-button-group button").removeClass('uk-active');
		$('div.uk-button-group button[title="' + $(this).attr('title') + '"]').addClass('uk-active');
	});

	$("input[type='checkbox']").on('change',function(e){
		var that = this;
		$(this)
			.closest('li')
			.find('div.uk-form-row select')
			.each(function(){
				if(that.checked){
					this.selectize.disable();
				}else{
					this.selectize.enable();
				}
				
			})
	});

	$("#between-hours-form").submit(function(e){
		// e.preventDefault()
		// if($('#between-hours-form input[name="name"]').val() == ''){
		// 	alert('Please give the schedule a name');
		// }
		// return false;
	})

	$("#duplicate-hours").on('click',function(e){
		e.preventDefault();
		var currentDay = $("#day-switcher li.uk-active").index();
		var calEnd = $("select.cal-end").eq(currentDay).val();
		var calStart = $("select.cal-start").eq(currentDay).val();
		var s = '';
		
		$('select').each(function(){
			if($(this).hasClass('cal-end') && $(this)[0].selectize !== undefined){
				$(this)[0].selectize.addItem(calEnd)
			}else if($(this)[0].selectize !== undefined){
				$(this)[0].selectize.addItem(calStart)
			}
		});
		$("div.uk-button-group button.uk-button").eq(0).trigger('click');
		setTimeout(function(){
			$("div.uk-button-group button.uk-button").eq(1).trigger('click');
			setTimeout(function(){
				$("div.uk-button-group button.uk-button").eq(2).trigger('click');
				setTimeout(function(){
					$("div.uk-button-group button.uk-button").eq(3).trigger('click');
					setTimeout(function(){
						$("div.uk-button-group button.uk-button").eq(4).trigger('click');
						setTimeout(function(){
							$("div.uk-button-group button.uk-button").eq(5).trigger('click');
							setTimeout(function(){
								$("div.uk-button-group button.uk-button").eq(6).trigger('click');
								setTimeout(function(){
									$("div.uk-button-group button.uk-button").eq(0).trigger('click');
								},50)
							},50)
						},50)
					},50)
				},50)
			},50)
		},50)	
	})
});