$(function(){
	var $modal = new $.UIkit.modal.Modal("#rejection-modal");
	var parentID = 0;
	var $row = '';
	$("#rejection-form").submit(function(e){
		e.preventDefault();
		$modal.hide();
		$row.remove();
		var data = {
			reason: $("#reason").val(),
			parent_id: parentID
		};
		$.post('/' + school + '/rejection/',data,function(data){
			//
		});
		return false;
	});

	$(".reject-parent").on('click',function(){
		parentID = $(this).attr('data-id');
		$row = $(this).closest('tr');
		$modal.show();
	});
});