$(function(){
	var alertWarning = function(message){
		$("#drop-warning")
			.html(message)
	    	.removeClass('uk-hidden')
	    	.delay(30000)
	    	.slideUp();
	}

	$('#drop-zone').filedrop({
		fallback_id: 'upload_button',   // an identifier of a standard file input element, becomes the target of "click" events on the dropzone
	    url: function(file){
	    	return '/' + school + '/kids/' + kid_id + '/upload/file/'
	    },              // upload handler, handles each file separately, can also be a function taking the file and returning a url
	    paramname: 'import_file',          // POST parameter name used on serverside to reference file, can also be a function taking the filename and returning the paramname
	    // withCredentials: true,          // make a cross-origin request with cookies
	    
	    headers: {          // Send additional request headers
	        'header': 'value'
	    },
	    error: function(err, file) {
	        switch(err) {
	            case 'BrowserNotSupported':
	            	alertWarning('browser does not support HTML5 drag and drop');
	                break;
	            case 'TooManyFiles':
	                alertWarning('You can only import 1 file.')
	                break;
	            case 'FileTooLarge':
	                alertWarning('That file is too large.')
	                break;
	            case 'FileTypeNotAllowed':
	                alertWarning('Please upload an jpg or png image file.')
	                break;
	            case 'FileExtensionNotAllowed':
	                alertWarning('Please upload an jpg or png image file.')
	                break;
	            default:
	                break;
	        }
	    },
	    allowedfiletypes: ['image/jpeg','image/pjpeg','image/png'],   // filetypes allowed by Content-Type.  Empty array means no restrictions
	    allowedfileextensions: ['.jpg','.jpeg','.png','.JPG'], // file extensions allowed. Empty array means no restrictions
	    maxfiles: 1,
	    maxfilesize: 20,    // max file size in MBs
	    dragOver: function() {
	        $("#drop-zone").addClass('uk-panel-box-primary');
	    },
	    dragLeave: function() {
	       $("#drop-zone").removeClass('uk-panel-box-primary');
	    },
	    docOver: function() {
	        $("body").css("background-color","#FF0000");
	        $("body").css("background-color","#FFFFFF");
	    },
	    docLeave: function() {
	        $("#drop-zone").removeClass('uk-panel-box-primary');
	        $("body").css("background-color","#FFFFFF");
	    },
	    drop: function() {
	        $("#drop-zone").removeClass('uk-panel-box-primary');
	    },
	    uploadStarted: function(i, file, len){
	    	$("#file-status").html('Upload started' + '<i class="uk-icon-refresh uk-icon-spin uk-icon-large"></i>');
	        // a file began uploading
	        // i = index => 0, 1, 2, 3, 4 etc
	        // file is the actual file of the index
	        // len = total files user dropped
	    },
	    uploadFinished: function(i, file, response, time) {
	        $("#file-status").text('File has been uploaded!');
	        if(response.status != 'OK'){
	        	console.log(response)
	        	$("#file-status").text("Something went wrong. We are trying to fix it right now. Try again later.");
	        	$("#img-file").addClass('uk-hidden')
	        }else{
	        	$("#drop-zone").remove();
	        	$("#profile-pic-area").append('<img class="uk-thumbnail" src="' + response.pic + '">')
	        }
	    },
	    progressUpdated: function(i, file, progress) {
	        $("#file-status").text('File uploading...' + progress + '% done');
	    },
	    globalProgressUpdated: function(progress) {
	        // progress for all the files uploaded on the current instance (percentage)
	        // ex: $('#progress div').width(progress+"%");
	    },
	    speedUpdated: function(i, file, speed) {
	        // speed in kb/s
	    },
	    rename: function(name) {
	        // name in string format
	        // must return alternate name as string
	    },
	    beforeEach: function(file) {
	        if(file.name.indexOf('xlsx') != -1 || file.name.indexOf('xls') != -1){
	        	$("#img-file")
	        	.removeClass('uk-hidden')
	        	.find('span')
	        	.text(file.name)
	        }else if(file.name.indexOf('csv') != -1){
	        	$("#csv-file")
	        	.removeClass('uk-hidden')
	        	.find('span')
	        	.text(file.name)
	        }
	    },
	    beforeSend: function(file, i, done) {
	        done();
	    },
	    afterAll: function() {
	        // runs after all files have been uploaded or otherwise dealt with
	    }
	})
})