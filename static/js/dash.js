$(function(){
	var $removed = '';
	$("#message-entire-school").on('click',function(e){
		e.preventDefault();
		$("#message-who").empty().append('<option value="school"></option>');
		$("#message-to").text('your entire school')
		$("#message-center").trigger('open');
	});

	$("a.sign-in-kid, a.sign-out-kid").on("click",function(e){
		e.preventDefault();
		var $this = $(this);
		var kidID = $this.attr('data-id');
		var outNum = 0;
		var out = true;
		if($this.hasClass('sign-in-kid')){
			outNum = 1;
			out = false
		}
		var sendData = {
			kid:kidID,
			status:outNum,
			parent:parent
		}

		$.post(postURL,sendData,function(data){
			if(data.status == 'OK'){
				$('img.profile-pic-thumb[data-id="' + kidID + '"]').removeClass('thumb-in');
				if(out){
					$this.find("i").removeClass('uk-icon-sign-out').addClass('uk-icon-sign-in');
					$this.removeClass('sign-out-kid')
						.addClass('sign-in-kid');
					$this.closest("div.group-kid").removeClass('kid-in').addClass('kid-out');
				}else{
					$('img.profile-pic-thumb[data-id="' + kidID + '"]').addClass('thumb-in')
					$this.find("i").removeClass('uk-icon-sign-in').addClass('uk-icon-sign-out');
					$this.removeClass('sign-in-kid')
						.addClass('sign-out-kid');
					$this.closest("div.group-kid").removeClass('kid-out').addClass('kid-in');
					
				}
				
			}
		});
	});

	$("#sign-in-school, #sign-out-school").on('click',function(e){
		var out = false;
		var word = 'in';
		var status = 1;
		if($(this).attr('id') == 'sign-out-school'){
			out = true;
			word = 'out';
			status = 0;
		}
		if(!confirm('Are you sure you want to sign ' + word + ' the entire school?')){
			return false;
		}else{
		}
		e.preventDefault();
		$("#loading").show();
		$.post(postURL,{kids:'all',
			status:status
		},function(data){
			$("#loading").hide();
			if(data.status == 'OK'){
				if(out){
					$("div.profile-pic img").removeClass('thumb-in');
					$("a.sign-out-kid i").removeClass('uk-icon-sign-out').addClass('uk-icon-sign-in');
					$("a.sign-out-kid").removeClass('sign-out-kid')
						.addClass('sign-in-kid');
					$("div.group-kid").removeClass('kid-in').addClass('kid-out');
				}else{
					$("div.profile-pic img").addClass('thumb-in');
					$("a.sign-in-kid i").removeClass('uk-icon-sign-in').addClass('uk-icon-sign-out');
					$("a.sign-in-kid").removeClass('sign-in-kid')
						.addClass('sign-out-kid');
					$("div.group-kid").removeClass('kid-out').addClass('kid-in');
				}
			}else{
				alert('There was a problem connecting to the server.')
			}
		});	
		
	});

	// $("a.add-activity-group").on('click',function(e){
	// 	e.preventDefault();
	// 	if($(this).hasClass('expanded-activities')){
	// 		$(this).removeClass('expanded-activities');
	// 		$(this).find('i').removeClass('uk-icon-caret-square-o-up').addClass('uk-icon-rocket');
	// 		$(this).closest('div.group-box').find('div.add-activity-panel').slideUp(300);	
	// 	}else{
	// 		$(this).addClass('expanded-activities');
	// 		$(this).find('i').removeClass('uk-icon-rocket').addClass('uk-icon-caret-square-o-up');
	// 		$(this).closest('div.group-box').find('div.add-activity-panel').slideDown(300);	
	// 	}
		
	// });

	$("a.group-send-message").on('click',function(e){
		e.preventDefault();
		$("#message_group").val($(this).attr('data-id'));
		$("#message-who").empty().append('<option value="group"></option>')
		$("#message-to").text(' all parents of children in ' + $(this).attr('data-name'))
		$("#message-center").trigger('open');
	});

	$("a.activity-type-group").on('click',function(e){
		e.preventDefault();
		var $this = $(this);
		$this.closest('div.add-activity-panel').slideUp(300);
		var groupID = $this.closest('div.group-box').attr('data-id');
		var activityID = $this.attr('data-id');
		$this.closest('div.group-box')
			.find('a.add-activity-group')
			.removeClass('expanded-activities')
			.find('i')
			.removeClass('uk-icon-caret-square-o-up')
			.addClass('uk-icon-rocket');
	});

	$("#add-activity-side-button").on('click',function(e){
		e.preventDefault();

	});

	var a = [];
	var populateNames = function(){
		a = [];
		$("div.kid-name a").each(function(e){
			a.push($(this).text().toLowerCase())
		});	
	}

	populateNames();
	

	var $container = $('div.kid-container');
	// initialize
	$container.nested({
		selector:'.group-kid',
		minWidth:100,
		resizeToFit: false,
		gutter:15,
		resizeToFitOptions: { 
			resizeAny: false
		}
	});
	$('div.crop-me').imgLiquid({
		fill:true,
		horizontalAlign:'center',
		verticalAlign:'center'
	});
	$("div.group-kid.size21 div.profile-pic").css("float","left");

	$("#search-tots").on('keyup',function(e){
		if($(this).val().length < 2){
			$("div.group-box").show();
			addKidsBackIn();
			$removed = [];
			populateNames();
			return;
		}
		var hideExcept = searchNames($(this).val().toLowerCase());
		if(hideExcept.length == 0){
			// $("div.group-box").show();
			// addKidsBackIn();
			// $removed = [];
			// populateNames();
			// return;	
		}
		$("div.group-kid").show();
		$new_removed = $("div.group-kid").filter(function(i){
			return $.inArray((i), hideExcept) == -1
		}).detach();
		$removed = _.union($.makeArray($new_removed),$removed);
		$("div.group-box").each(function(){
			var $this = $(this);
			if(!$this.find("div.group-kid:visible").length){
				$this.hide();
			}
		});
		populateNames();

		$container.nested("refresh");

	});

	function searchNames(getName){
		var returnA = [];
		for (var i = a.length - 1; i >= 0; i--) {
			var ind = a[i].indexOf(getName);
			if(ind != -1){
				returnA.push(i);
			}
		};
		return returnA;
	}

	var addKidsBackIn = function(){
		if(!$removed || !$removed.length)return;
		populateNames();
		_.each($removed,function(el,i,a){
			var $this = $(el);
			var groupID = $this.attr('data-group');
			$("div.group-box[data-id='" + groupID + "'] div.kid-container").append($this);
		})
		$container.nested("refresh");
	}

	$("#show-hold").on('click',function(e){
		e.preventDefault();
		$("#hold-kids").show();
		$('html, body').animate({scrollTop:$(document).height()}, 'slow');
		// window.scrollTo(0,document.body.scrollHeight);
	});

	$("#hide-hold-kids").on('click',function(e){
		e.preventDefault();
		$("#hold-kids").hide();
		$('html, body').animate({scrollTop:0}, 'slow');
	});
});