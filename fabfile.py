from __future__ import with_statement
from fabric.api import local, settings, abort, run, cd, env, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists

env.hosts =['192.168.1.3']

def prepare_deploy(branch="master", m='fabric commit'):
	local("pip freeze > requirements.txt")
	with settings(warn_only=True):
		local("git add . --all && git commit -m '%s'" % m)
		local("git push origin %s" % branch)

def reset_db():
	code_dir = '/home/jschatz1/projects/py/faycare'
	with cd(code_dir):
		if(exists('/home/jschatz1/gunicorn.pid')):
			run("kill `cat /home/jschatz1/gunicorn.pid`")
		run("sh reset.sh")

def restart():
	code_dir = '/home/jschatz1/projects/py/faycare'
	with cd(code_dir):
		if(exists('/home/jschatz1/gunicorn.pid')):
			run("kill `cat /home/jschatz1/gunicorn.pid`")
		run("gunicorn serve:app -p /home/jschatz1/gunicorn.pid -b 127.0.0.2:8000 -w 4")

def reset_rerun():
	reset_db()
	restart()

def deploy(branch="master",reset_db=False):
	code_dir = '/home/jschatz1/projects/py/faycare'
	with cd(code_dir):
		run("git fetch")
		run("git checkout %s" % branch)
		run("git pull origin %s" % branch)
		if(exists('/home/jschatz1/gunicorn.pid')):
			run("kill `cat /home/jschatz1/gunicorn.pid`")
		sudo("pip install -r requirements.txt")
		if reset_db:
			run("sh reset.sh")
		run("gunicorn serve:app -p /home/jschatz1/gunicorn.pid -b 127.0.0.2:8000 -w 4")

def final_deploy(branch="master",m='fabric commit',reset_db=False):
	prepare_deploy(branch=branch,m=m)
	deploy(branch=branch,reset_db=reset_db)