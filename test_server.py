from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin
import os

app = Flask(__name__)

# app.config.from_object('faycare.config.DevConfig')
# app.logger.info("Config: Development")
app.config.from_object('faycare.config.ProductionConfig')
app.logger.info("Config: Production")

def register_blueprints():
	import faycare.assets
	from faycare.views.front import front
	from faycare.views.back import back
	from faycare.views.api import api
	import faycare.tasks

	app.register_blueprint(front)
	app.register_blueprint(back)
	app.register_blueprint(api)

def init_error_codes():
	@app.errorhandler(404)
	def page_not_found(e):
		return render_template('status/404.html')

@app.context_processor
def utility_processor():
	def format_price(amount, currency=u'$'):
		return u'{1}{0:.2f}'.format(float(amount), currency)
	return dict(format_price=format_price)

init_error_codes()
register_blueprints()

if __name__ == "__main__":
    app.run()