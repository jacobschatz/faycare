from faycare.models import *
from faycare.views import money_helpers
from faycare import apikeys
import stripe

@celery.task()
def bill_all_schools():
	for school in School.query.all():
		if school.billing_on:
			bill_kids_in_school.delay(school.kids.all(),school.id)

	def on_failure(self, *args, **kwargs):
		print 'failure in bill_all_schools';

@celery.task()
def bill_kids_in_school(kids,school_id):
	for kid in kids:
		create_kid_bill.delay(kid.id,school_id)

	def on_failure(self, *args, **kwargs):
		print 'failure in bill_kids_in_school';

@celery.task()
def create_kid_bill(kid_id,school_id):
	money_helpers.bill_next_month(kid_id,school_id)

	def on_failure(self, *args, **kwargs):
		print 'failure in create_kid_bill';

@celery.task()
def extra_kids_in_school_charge():
	for company in Company.query.all():
		bill_for_extra_kids.delay(company.id)

	def on_failure(self, *args, **kwargs):
		print 'failure in extra_kids_in_school_charge';

@celery.task()
def bill_for_extra_kids(company_id):
	company = Company.query.get(company_id)
	limit = company.plan.limit
	current = company.total_kids
	overage = company.plan.overage
	plan_name = company.plan.name

	if plan_name != 'free':
		admin = company.admin
		has_uuid = False
		cust_uuid = 0
		if admin.customer:
			if admin.customer.uuid:
				has_uuid = True
				cust_uuid = admin.customer.uuid

		if has_uuid and current > limit:
			over_kids = current - limit
			overage_charge = overage * over_kids
			stripe.api_key = apikeys.stripe_api_secret
			overage_charge_message = "%s plan. %i kids total. %i extra kids at $%s per kid." % (plan_name,current,over_kids,str(overage))

			stripe.InvoiceItem.create(
			    customer=cust_uuid,
			    amount=int(overage_charge * 100), # in cents
			    currency="usd",
			    description=overage_charge_message)

	def on_failure(self, *args, **kwargs):
		print 'failure in bill_for_extra_kids';
