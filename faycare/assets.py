from faycare.models import assets
from flask.ext.assets import Environment, Bundle

js = Bundle('js/lib/jquery-1.8.0.min.js', 
		'js/lib/uikit.min.js',
		'js/lib/jquery.timeago.js',
		'js/lib/selectize.min.js',
		'js/base.js', 
		'js/widgets.js',
	filters='jsmin', output='gen/packed.js')
assets.register('js_all', js)

mark_down_js = Bundle('js/lib/codemirror.js',
	'js/lib/markdown.js',
	'js/lib/overlay.js',
	'js/lib/xml.js',
	'js/lib/gfm.js',
	'js/lib/marked.js',
	'js/lib/markdownarea.js',
	filters='jsmin', output='gen/markdown.js')
assets.register('markdown_js', mark_down_js)

graphing_js = Bundle('js/lib/jquery.ui.core.js',
		'js/lib/jquery.ui.datepicker.js',
		'js/lib/jquery-ui.multidatespicker.js',
		'js/lib/timepicker.min.js',
		'js/lib/underscore-min.js',
		'js/lib/d3.min.js',
		'js/lib/nv.d3.min.js',
		'js/attendance_graph.js',
	filters='jsmin', output='gen/graphing.js')
assets.register('graphing', graphing_js)

multi_date_js = Bundle('js/lib/jquery.ui.core.js',
		'js/lib/jquery.ui.datepicker.js',
		'js/lib/jquery-ui.multidatespicker.js',
		'js/lib/timepicker.min.js',
		'js/lib/underscore-min.js',
	filters='jsmin', output='gen/calendar.js')
assets.register('calendar', multi_date_js)

attendance_js = Bundle('js/lib/jquery.color.min.js',
		'js/touch.js',
		'js/attendance.js',
	filters='jsmin', output='gen/attendance.js')
assets.register('attendance', attendance_js)

import_js = Bundle('js/lib/jquery.filedrop.js',
		'js/import.js',
	filters='jsmin', output='gen/import.js')
assets.register('importance', import_js)

kid_upload_js = Bundle('js/lib/jquery.filedrop.js',
		'js/kid_upload.js',
		'js/lib/upload.min.js',
		'js/lib/isotope.min.js',
		'js/lib/imagesloaded.js',
	filters='jsmin', output='gen/kid.js')
assets.register('kid_upload', kid_upload_js)

kids_js = Bundle('js/lib/snap.svg-min.js',
		'js/kid-svg.js',
	filters='jsmin', output='gen/kids.js')
assets.register('kids', kids_js)

date_css = Bundle('css/lib/jquery-ui.min.css',
	'css/lib/jquery.ui.datepicker.min.css',
	'css/lib/mdp.css',
	'css/lib/uikit.addons.min.css',
	filters='cssmin',output='gen/calendar.css')
assets.register('css_calendar',date_css)

front_css = Bundle('css/lib/uikit.min.css', 'css/styles.css',
	filters='cssmin', output="gen/all.css")
assets.register('css_front', front_css)

back_css = Bundle('css/lib/selectize.css',
	'css/lib/selectize.default.css',
	'css/lib/uikit.min.css', 
	'css/back-styles.css',
	filters='cssmin', output="gen/back-all.css")
assets.register('css_back', back_css)