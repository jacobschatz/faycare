from flask import Blueprint, render_template, request, flash, redirect, jsonify, g, url_for, abort
from flask.views import MethodView
from flask_wtf import Form
from flask.ext.mail import Mail, Message
from flask.ext.security import current_user, login_required, roles_required, roles_accepted, forms
from wtforms import TextField, PasswordField, validators, \
    SubmitField, HiddenField, BooleanField, ValidationError, Field, \
    RadioField, SelectMultipleField, SelectField, TextAreaField,DecimalField, \
    FieldList, IntegerField
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from wtforms.ext.dateutil.fields import DateTimeField
from sqlalchemy.sql import exists
from sqlalchemy import func, and_, desc, or_, asc
from flask.ext.login import logout_user, login_user
from flask.ext.babel import format_datetime
import datetime
import pytz
import re
from flask.ext.security.utils import encrypt_password
from faycare.models import *
import mandrill
from faycare.timezones import timezones
import faycare.apikeys
from faycare.views import money_helpers

back = Blueprint('back', __name__, template_folder="templates", url_prefix='/<school_slug>')

whitelist_html = ['a','h1','h2','h3','h4','h5','h6','span','select','option','div','ul','li','ol','em','strong','img','del','p','blockquote','b','strong','br','input','label']
whitelist_attr = attrs = { '*': [],'select': ['name', 'id', 'size','class'] ,'option':['value'], 'input': ['type', 'name', 'id', 'size','class','value'], 'label': ['for','class'] }
weekday_list = ['mon','tues','weds','thurs','fri','sat','sun']
months_list = [(0,'Month'),(1,'Jan'),(2,'Feb'),(3,'Mar'),(4,'Apr'),(5,'May'),(6,'Jun'),(7,'Jul'),(8,'Aug'),(9,'Sep'),(10,'Oct'),(11,'Nov'),(12,'Dec')]
day_list = [(r,r) for r in range(0,32)]
year_list = [(r,r) for r in range(datetime.datetime.today().year,datetime.datetime.today().year - 120,-1)]

def c_bnc():
	for b in Bill.query.all():
		db.session.delete(b)
		db.session.commit()

	for b in Credit.query.all():
		db.session.delete(b)
		db.session.commit()

	for i in Invoice.query.all():
		db.session.delete(i)
		db.session.commit()

def add_school_slug(endpoint, values):
    values.setdefault('school_slug', g.school_slug)

def pull_school_slug(endpoint, values):
		g.current_school = None
		g.school_slug = values.pop('school_slug')

def format_price(amount, currency=u'$'):
	return u'{1}{0:.2f}'.format(amount, currency)

def before_back():

	if request.headers.get('X-Requested-With') != 'XMLHttpRequest':
		check_browser()
		if not current_user.is_authenticated():
			return redirect('/login/')
		if not current_user.schools.all():
			return redirect('/setup/')
		if not current_user.roles:
			return redirect('/setup/')

	else:
		if not current_user.is_authenticated():
			return jsonify({'status':'NOGOOD','message':'No longer signed in.'})

	g.message_form = MessageForm()

	found_school = School.query.filter_by(u_name=g.school_slug).first()

	if found_school in current_user.schools.all():
		g.current_school = found_school
	else:
		#trying to access another school
		g.current_school = current_user.schools[0]
		return redirect('/%s/' % g.current_school.u_name)

	g.is_attendance = current_user.is_attendance()

	g.testing = g.current_school.testing

	g.is_parent = current_user.is_parent()
	g.is_director = current_user.is_director()
	g.is_teacher = current_user.is_teacher()

	if current_user.is_le_admin():
		g.is_admin = True
		g.pending_parents = User.query.filter(
			User.schools.contains(
				g.current_school
			),User.roles.contains(
				user_datastore.find_role('parent')
			),User.pending == True).count()
	else:
		g.is_admin = False

def kids_in():
	pass

def sizeof_fmt(num):
	for x in ['bytes','KB','MB','GB']:
		if num < 1024.0 and num > -1024.0:
			return "%3.1f%s" % (num, x)
		num /= 1024.0
	return "%3.1f%s" % (num, 'TB')

days = ['mon','tues','weds','thurs','fri','sat','sun']

billing_units = [(0,'days'),(1,'months')]
billing_units_display = ['days','months','hours']

back.before_request(before_back)
back.url_value_preprocessor(pull_school_slug)
back.url_defaults(add_school_slug)

def get_group_query_factory():
	return g.current_school.groups

def get_allergens_query_factory():
	return g.current_school.allergens

def get_kids_query_factory():
	return g.current_school.kids

def get_kids_with_groups_factory():
	return g.current_school.kids.filter(and_(Kid.groups.any(),Kid.hold==False))

def get_people_query_factory(role):
	return User.query.filter(
			User.schools.contains(
				g.current_school
			),
			User.roles.contains(
				user_datastore.find_role(role)
			)
		)

def get_people(role):
	return User.query.filter(
				User.schools.contains(
					g.current_school
				),
				User.roles.contains(
					user_datastore.find_role(role)
				)
			).all()

def get_people_base(role):
	return User.query.filter(
				User.schools.contains(
					g.current_school
				),
				User.roles.contains(
					user_datastore.find_role(role)
				)
			)

def get_order_priority_query_factory():
	return OrderPriority.query.all()

def get_order_type_query_factory():
	return OrderType.query.all()

def get_parents_query_factory():
	return get_people_query_factory('parent')

def get_staff_query_factory():
	return User.query.filter(
		User.schools.contains(
			g.current_school
		),
		or_(User.roles.contains(
			user_datastore.find_role('teacher')
		),
		User.roles.contains(
			user_datastore.find_role('director')
		),
		User.roles.contains(
			user_datastore.find_role('admin')
		))
	)

def get_parents():
	return get_people('parent')

def get_staff():
	parent = user_datastore.find_role('parent')
	attendance = user_datastore.find_role('attendance')
	return User.query.filter(
				User.schools.contains(
					g.current_school
				),
				~User.roles.contains(
					parent
				)
			).all()

def get_attendance():
	return get_people('attendance')

def get_message_obj(template,subject,to_email,to_name,tags):
	return {
			'html':template,
			'subject':subject,
			'from_email':'jacob@gototgo.com',
			'to':[{
					'email':to_email,
					'name': to_name
				}],
			'view_content_link':False,
			'tags':tags
		}

def get_message_obj_multi(template,subject,to_emails,tags):
	return {
			'html':template,
			'subject':subject,
			'from_email':'info@%s.com' % faycare.apikeys.app_name,
			'to':[{'email':u.email,'name':u.person.first_name} for u in to_emails],
			'view_content_link':False,
			'tags':tags
		}

def canViewKid(kid_id):
	if current_user.is_parent():
		if current_user.kids.filter(Kid.id == kid_id).count():
			return True
		else:
			return False
	elif g.is_admin or g.is_teacher or g.is_director:
		if Kid.query.filter(Kid.school == g.current_school,Kid.id==kid_id).count():
			return True
		else:
			return False
	else:
		return False

def in_out_count():
	return Kid.query.filter(Kid.school == g.current_school,Kid.status == 1).count()

def allowed_img_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in ['jpg','jpeg','png']

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1] in ['xlsx','xls','csv']

def get_schedule(schedule_id, schedule_type):
	schedule = None
	if schedule_type == 'between_hours':
		schedule = BetweenSchedule.query.get(schedule_id)
	elif schedule_type == 'one_for_all':
		schedule = OneForAllSchedule.query.get(schedule_id)
	elif schedule_type == 'hourly':
		schedule = HourlySchedule.query.get(schedule_id)
	else:
		schedule = CalendarSchedule.query.get(schedule_id)
	return schedule

def timerange(start, stop, step = datetime.timedelta(minutes = 15)):
	t = datetime.datetime(2000, 1, 1, start.hour, start.minute)
	stop = datetime.datetime(2000, 1, 1 if start <= stop else 2, stop.hour, stop.minute)
	while t <= stop:
		yield t.time()
		t += step

def slugify(value):
	import unicodedata
	import re
	value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
	value = unicode(re.sub('[^\w\s-]', '', value).strip().lower())
	value = unicode(re.sub('[-\s]+', '-', value))
	return value

def ftimerange(start, stop, step = datetime.timedelta(minutes = 15)):
	for t in timerange(start, stop, step):
		yield t.strftime('%I:%M%p')

def ftimerangetup(start, stop, step = datetime.timedelta(minutes = 15)):
	for t in timerange(start, stop, step):
		yield (t,t.strftime('%I:%M%p'))

def check_browser():
	browser = request.user_agent.browser
	version = request.user_agent.version and int(request.user_agent.version.split('.')[0])
	platform = request.user_agent.platform
	uas = request.user_agent.string

	if browser and version:
		if (browser == 'msie' and version < 10) \
		or (browser == 'firefox' and version < 4) \
		or (platform == 'android' and browser == 'safari' and version < 534) \
		or (platform == 'iphone' and browser == 'safari' and version < 7000) \
		or ((platform == 'macos' or platform == 'windows') and browser == 'safari' and not re.search('Mobile', uas) and version < 534) \
		or (re.search('iPad', uas) and browser == 'safari' and version < 7000) \
		or (platform == 'windows' and re.search('Windows Phone OS', uas)) \
		or (browser == 'opera') \
		or (re.search('BlackBerry', uas)):
			flash('Wow, your browser is ancient! <br />This site (and the rest of the internet) will not work well unless you get with the times. <a href="/support/browser/">Click here for more info.</a>')

def get_tuple_for_day(start, stop):
	times = []
	for t in ftimerangetup(start,stop):
		times.append(t)
	return times

def set_form_per_school_hours(form):
	for day in days:
		if not getattr(g.current_school.school_hours,'closed_' + day):
			getattr(form,day + '_start').choices = get_tuple_for_day(getattr(g.current_school.school_hours,day + '_start'),getattr(g.current_school.school_hours,day + '_end'))
			getattr(form,day + '_end').choices = get_tuple_for_day(getattr(g.current_school.school_hours,day + '_start'),getattr(g.current_school.school_hours,day + '_end'))

def get_kid_schedule(kid):
	if kid.between_schedule:
		return kid.between_schedule
	elif kid.one_for_all_schedule:
		return kid.one_for_all_schedule
	elif kid.hourly_schedule:
		return kid.hourly
	elif kid.calendar_schedule:
		return kid.calendar_schedule
	else:
		return None

def get_late_fees(kid,dt):
	if g.current_school.grace == -1:
		return 0
	schedule = get_kid_schedule(kid)

	if not schedule:
		return 0

	if schedule.late_price == 0:
		return 0

	return 100

def code_check(form,field):
	try:
		int(field.data)
	except ValueError:
		raise ValidationError('Code must contain only numbers.')

class InView(MethodView):
	def get(self):
		kids = Kid.query.filter(Kid.school == g.current_school,Kid.status==1).all()
		return render_template('back/in_out.html',status='In',kids=kids)

class OutView(MethodView):
	def get(self):
		kids = Kid.query.filter(Kid.school == g.current_school,Kid.status==0).all()
		return render_template('back/in_out.html',status='Out',kids=kids)

class HomeView(MethodView):
	def get(self):
		if current_user.is_attendance():
			return redirect(url_for('back.attendance'))
		if not current_user.person.first_name:
			flash('Before we continue, we need to get a little more information.')
			return redirect(url_for('back.settings'))
		kids_form = KidsForm()

		kid_message_form = MessageForm()
		if g.is_admin or g.is_director:
			kids_in = 0
			kids_out = 0
			kids_in = Kid.query.filter(and_(Kid.school == g.current_school,Kid.status==1,Kid.hold == False)).count()
			kids_out = Kid.query.filter(and_(Kid.school == g.current_school,Kid.status==0,Kid.hold == False)).count()

			return render_template('back/dash.html',
				kids_in=kids_in,
				kids_out=kids_out,
				hold_kids=Kid.query.filter(and_(Kid.school == g.current_school,Kid.hold == True)),
				activity_types=ActivityType.query.all(),
				parent_form=PersonalInfoForm(),
				has_groups=Group.query.filter(Group.school==g.current_school).count(),
				has_allergens=Allergen.query.filter(Allergen.school==g.current_school).count(),
				has_parents=get_people_base('parent').count(),
				unassigned_kids=Kid.query.filter(Kid.school==g.current_school,~Kid.groups.any()).all(),
				socket_connection=config['SOCKET_CONNECTION'],
				kid_message_form=kid_message_form,
				groups= Group.query.filter(Group.school == g.current_school).all(),
				kids_form=kids_form)
		elif current_user.is_teacher():
			kid_message_form = kid_message_form,
			kids_form = None
			kids = None
			kids_in = []
			kids_out = []
			for group in current_user.groups:
				kids_in.append(group.kids.filter(Kid.status == 1).count())
				kids_out.append(group.kids.filter(Kid.status == 0).count())
			return render_template('back/teacher_dash.html',
				kids_in=kids_in,
				kids_out=kids_out)
		elif current_user.is_parent():
			if len(current_user.kids.all()) == 1:
				return redirect(url_for('back.kids_show',kid_id=current_user.kids.first().id))
			return render_template('back/parent_dash.html',
				kids_form = ParentsKidsForm())

class KidsEditView(MethodView):
	@roles_accepted('admin','director')
	def get(self, kid_id):
		if canViewKid(kid_id):
			kid = Kid.query.get(kid_id)
			kids_form = KidsForm(request.form,obj=kid)
			return render_template('back/kid_edit.html',
				kid=kid,
				kids_form=kids_form,
				highlight_group='highlight' in request.args and request.args['highlight'] == 'group'
			)
		else:
			return redirect(url_for('back.home'))

	@roles_accepted('admin','director')
	def post(self,kid_id):
		if canViewKid(kid_id):
			kid = Kid.query.get(kid_id)
			old_groups = set(kid.groups.all())
			kids_form = KidsForm(request.form)
			if kids_form.validate():
				if kids_form.scholarship.data == '':
					kids_form.scholarship.data = 0;
				if kids_form.govt.data == '':
					kids_form.govt.data = 0;
				if kids_form.extra_credit.data == '':
					kids_form.extra_credit.data = 0;
				kids_form.populate_obj(kid)
				if kids_form.groups.data:
					kid.current_group = kids_form.groups.data[0].name
				else:
					kid.current_group = 'No group'
				db.session.commit()

				if old_groups != set(kid.groups.all()):
					if g.current_school.billing_on:
						money_helpers.prorate_and_bill(kid)
						flash('Kid updated successfully. Check your bills for newly generated bills.')
					else:
						flash('Kid updated successfully.')
				else:
					flash('Kid updated successfully.')
				return redirect(url_for('back.home'))
			else:
				return render_template('back/kid_edit.html',
				kid=kid,
				kids_form=kids_form)
		else:
			return redirect(url_for('back.home'))

class KidsNewView(MethodView):
	@roles_accepted('admin','director')
	def get(self):

		return render_template('back/kids_new.html',kids_form=KidsForm())

class KidsShowView(MethodView):
	def get(self, kid_id):
		import json
		from dateutil.relativedelta import relativedelta

		if canViewKid(kid_id):
			locked_months = []
			kid = Kid.query.get(kid_id)
			has_calendar = False
			cal_days = []
			if kid.calendar_schedule:
				has_calendar = True
				last_month = datetime.datetime.utcnow() - relativedelta(months=1,day=1)
				two_month_from_now = datetime.datetime.utcnow() + relativedelta(months=3,day=1)
				dates = CalendarAttendanceDay.query.filter(
				CalendarAttendanceDay.kid==kid,
				and_(
				CalendarAttendanceDay.date >= last_month,CalendarAttendanceDay.date <= two_month_from_now )
				).all()
				json_dates = []
				locked_months = []
				for date in dates:
					if date.approved:
						if not date.date.month in locked_months:
							locked_months.append(date.date.month)
					json_dates.append(date.date.strftime('%m/%d/%Y'))
				cal_days = json.dumps(json_dates)

			today = datetime.datetime.today()
			today = today - datetime.timedelta(hours=today.hour,minutes=today.minute,seconds=today.second,microseconds=today.microsecond)
			week_dates = [today + datetime.timedelta(days=i) for i in xrange(0 - today.weekday(), 7 - today.weekday())]
			attendance = Attendance.query.filter(Attendance.kid == kid).order_by(Attendance.time.desc()).limit(10)
			weekly_schedule = WeeklyHour.query.filter(
				or_(
					and_(
						WeeklyHour.start_time >= week_dates[0],
						WeeklyHour.end_time >= week_dates[0],
						WeeklyHour.kid == kid
						),
					and_(
						WeeklyHour.start_time <= week_dates[-1],
						WeeklyHour.end_time <= week_dates[-1],
						WeeklyHour.kid == kid
						)
					)
				).all()

			weekly_schedule_with_dates = []
			for day in week_dates:
				if weekly_schedule:
					matched = False
					for schedule_day in weekly_schedule:
						if schedule_day.start_time.date() == day.date():
							weekly_schedule_with_dates.append({'date':day.date(),'schedule_day':schedule_day})
							matched = True
							break;
					
					if not matched:
						weekly_schedule_with_dates.append({'date':day.date(),'schedule_day':None})

				else:
					weekly_schedule_with_dates.append({'date':day.date(),'schedule_day':None})

			photos = kid.photos[-20:]

			return render_template('back/kid_show.html',kid=kid,
				photos=photos,
				weekly_schedule_with_dates=weekly_schedule_with_dates,
				a_date=weekly_schedule_with_dates[0]['date'].strftime('%Y-%m-%d'),
				activities=kid.activities.order_by(Activity.id.desc()).limit(10),
				month=datetime.datetime.utcnow().month,
				year=datetime.datetime.utcnow().year,
				locked_months=json.dumps(locked_months),
				cal_days=cal_days,
				has_calendar=has_calendar,
				attendance=attendance)
		else:
			return redirect(url_for('back.home'))

	@roles_accepted('admin','director')
	def delete(self, kid_id):
		if canViewKid(kid_id):
			kid = Kid.query.get(kid_id)
			db.session.delete(kid)
			db.session.commit()
		return 'OK'

class KidsRemoveFile(MethodView):
	@roles_accepted('admin','director','teacher','parent')
	def post(self, kid_id):
		kid = Kid.query.get(kid_id)
		if kid:
			kid.profile_pic_small = None
			kid.profile_pic_smallish = None
			kid.profile_pic_med = None
			kid.profile_pic_large = None
			db.session.commit()
			return jsonify({'status':'OK'})
		else:
			return jsonify({'status':'NOGOOD'})

class KidsUploadFile(MethodView):
	def post(self, kid_id):
		from werkzeug import secure_filename
		import os
		from boto.s3.connection import S3Connection
		from boto.s3.key import Key
		import mimetypes
		import time
		import base64
		import uuid
		from PIL import Image, ExifTags

		kid = Kid.query.get(kid_id)

		is_activity_photo = False
		print 'request.endpoint ' + request.endpoint
		if request.endpoint == 'back.kids_upload_activity_file':
			is_activity_photo = True

		if is_activity_photo:
			file = request.files['files[]']
		else:
			file = request.files['import_file']


		if file and allowed_img_file(file.filename):

			to_upload = []
			temp_file = os.path.join(config['UPLOAD_FOLDER_TEMP'], secure_filename(file.filename))
			file.save(temp_file)
			fileName, ext = os.path.splitext(temp_file)
			image_filename = slugify(unicode('_'.join([g.current_school.name,kid.full_name,str(time.time()),base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')])))

			new_path = os.path.join(config['UPLOAD_FOLDER_TEMP'], ''.join([image_filename,ext]))
			os.rename(temp_file,new_path)
			for size in config['IMAGE_SIZES']:
				try:
					im =  Image.open(new_path)
				except:
					return

				for orientation in ExifTags.TAGS.keys() :
					if ExifTags.TAGS[orientation]=='Orientation' : break
				
				if hasattr(im, '_getexif'):
					try:
						exif=dict(im._getexif().items())
					except:
						exif = {}

					if exif:
						try:
							if exif[orientation] == 3 :
								im=im.rotate(180, expand=True)
							elif exif[orientation] == 6 :
								im=im.rotate(270, expand=True)
							elif exif[orientation] == 8 :
								im=im.rotate(90, expand=True)
						except:
							pass

				im.thumbnail((config['IMAGE_SIZES'].get(size),config['IMAGE_SIZES'].get(size)),
					Image.ANTIALIAS)
				new_name = image_filename + '_' + size + '.jpg'
				new_filename = image_filename + '_' + size
				new_name_without_path = new_filename
				new_name = os.path.join(config['UPLOAD_FOLDER_TEMP'],new_name)
				to_upload.append({'size' : size, 'path' : new_name, 'name' : new_filename, 'sans_path' : new_name_without_path })
				im.save(new_name, "JPEG")

			os.remove(new_path)

			mime = mimetypes.guess_type(to_upload[0].get('path'))[0]
			conn = S3Connection(faycare.apikeys.AWSAccessKeyId,faycare.apikeys.AWSSecretKey)

			if not current_user.bucket:
				user_bucket = slugify(unicode('_'.join([faycare.apikeys.AWSAccessKeyId,current_user.email])))
				bucket = conn.create_bucket(user_bucket)
				current_user.bucket = user_bucket
				db.session.commit()
			else:
				bucket = conn.get_bucket(current_user.bucket)

			if is_activity_photo:
				activity_photo = ActivityPhoto()
				activity_photo.kids.append(kid)
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				activity_photo.time = dt

			for single_upload in to_upload:
				k = Key(bucket)
				k.key = single_upload.get('name')
				k.set_metadata("Content-Type", mime)
				k.key = single_upload.get('name')
				full_uploaded_path = 'https://s3.amazonaws.com/%s/%s' % (bucket.name,single_upload.get('sans_path'))
				if is_activity_photo:
					setattr(activity_photo,'pic_' + single_upload.get('size'),full_uploaded_path)
				else:
					setattr(kid,'profile_pic_' + single_upload.get('size'),full_uploaded_path)
				k.set_contents_from_filename(single_upload.get('path'))
				os.remove(single_upload.get('path'))
				k.set_acl("public-read")


			if is_activity_photo:
				db.session.add(activity_photo)

			db.session.commit()
			if is_activity_photo:
				return jsonify({'status' : 'OK',
					'pic' : activity_photo.pic_smallish,
					'large':url_for('back.kids_photos',
						kid_id=kid.id,
						photo_id=activity_photo.id,
						size_id='large'),
					'id':activity_photo.id})
			else:
				return jsonify({'status' : 'OK','pic' : kid.profile_pic_med})
		return jsonify({'status':'NOGOOD'})

class GroupsActivitiesNew(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,group_id):
		activity_form = ActivityForm()
		group = Group.query.get(group_id)
		return render_template('back/group_activity_new.html',
			group=group,
			activity_form=activity_form,
			activity_types=ActivityType.query.all())

	@roles_accepted('admin','teacher','director')
	def post(self, group_id):
		group = Group.query.get(group_id)
		activity_form = ActivityForm(request.form)
		if activity_form.validate():
			for kid in group.kids:
				activity = Activity()
				activity.kid = kid
				activity.activity_type = ActivityType.query.get(request.form['activity_type'])
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				activity.time = dt
				activity.school = g.current_school
				activity.staff = current_user
				activity.description = request.form['description']
				db.session.add(activity)
			db.session.commit()
			flash('Activity added for %s!' % ', '.join([k.first_name for k in group.kids]))
			return redirect(url_for('back.home'))

class KidsActivitiesNew(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,kid_id):
		if canViewKid(kid_id):
			activity_form = ActivityForm()
			kid = Kid.query.get(kid_id)
			return render_template('back/kid_activity_new.html',
				kid=kid,
				activity_form=activity_form,
				activity_types=ActivityType.query.all())

	@roles_accepted('admin','teacher','director')
	def post(self,kid_id):
		from twilio.rest import TwilioRestClient

		if canViewKid(kid_id):
			kid = Kid.query.get(kid_id)
			activity_form = ActivityForm(request.form)
			if activity_form.validate():
				activity = Activity()
				activity.kid = kid
				activity.activity_type = ActivityType.query.get(request.form['activity_type'])
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				activity.time = dt
				activity.school = g.current_school
				activity.staff = current_user
				activity.description = request.form['description']
				db.session.commit()
				flash('Activity added!')

				if not g.testing:
					message = '%s just added an activity for %s: %s' % (current_user.person.first_name,kid.first_name,activity.activity_type.name)

					for user in kid.parents:
						if user.person.cell_phone:
							account_sid = faycare.apikeys.twilio_account_sid
							auth_token  = faycare.apikeys.twilio_auth_token
							client = TwilioRestClient(account_sid, auth_token)
							message = client.sms.messages.create(body=message,
							to=user.person.cell_phone,
							from_=faycare.apikeys.twilio_tele)

				return redirect(url_for('back.kids_show',kid_id=kid.id))
			else:
				return render_template('back/kid_activity_new.html',
					kid=kid,
					activity_form=activity_form,
					activity_types=ActivityType.query.all())

class KidsPhotos(MethodView):
	def get(self,kid_id,photo_id,size_id=None):
		kid = Kid.query.filter(Kid.id==kid_id,Kid.school==g.current_school).first()
		if kid:
			photo = ActivityPhoto.query.filter(ActivityPhoto.kids.contains(kid),ActivityPhoto.id==photo_id).first()
			if photo:
				if size_id:
					if size_id == 'small':
						return jsonify({'status':'OK','pic':photo.pic_small})
					elif size_id == 'smallish':
						return jsonify({'status':'OK','pic':photo.pic_smallish})
					elif size_id == 'med':
						return jsonify({'status':'OK','pic':photo.pic_med})
					elif size_id == 'large':
						return jsonify({'status':'OK','pic':photo.pic_large})
					else:
						return jsonify({'status':'OK','pic':photo.pic_smallish})
			else:
				return jsonify({'status':'NOGOOD'})	
		else:
			return jsonify({'status':'NOGOOD'})

class KidsView(MethodView):
	@roles_accepted('admin','parent','director')
	def post(self):
		if g.current_school.kids.count() >= g.current_school.company.plan.limit and g.current_school.company.plan.name == 'free':
			flash('You have reached your plan limit. You can upgrade under settings/account. Children were not added.')
			return redirect(url_for('back.home'))
		if g.is_admin or g.is_director:
			kids_form = KidsForm(request.form)
			if kids_form.validate():
				kid = Kid()
				kid.first_name = kids_form.first_name.data
				kid.middle_name = kids_form.middle_name.data
				kid.last_name = kids_form.last_name.data
				kid.school = g.current_school
				kid.status = 0
				kid.status_time = datetime.datetime.utcnow()
				kid.parents = kids_form.parents.data
				kid.groups = kids_form.groups.data
				kid.scholarship = kids_form.scholarship.data
				kid.govt = kids_form.govt.data
				kid.extra_credit = kids_form.extra_credit.data
				kid.allergens = kids_form.allergens.data
				if kids_form.groups.data:
				 	kid.current_group = kid.groups[0].name
				db.session.commit()
				return redirect(url_for('back.home'))
			return render_template('back/kids_new.html',
				kids_form=kids_form,
				errors=True)
		if current_user.is_parent():
			kids_form = ParentsKidsForm(request.form)
			if kids_form.validate():
				kid = Kid()
				kid.first_name = kids_form.first_name.data
				kid.middle_name = kids_form.middle_name.data
				kid.last_name = kids_form.last_name.data
				kid.school = g.current_school
				kid.pending = True
				kid.parents.append(current_user)
				kid.status = 0
				kid.status_time = datetime.datetime.utcnow()
				db.session.commit()
				return redirect(url_for('back.home'))
			return render_template('back/kids_new.html',
				kids_form=kids_form,
				errors=True)

class KidsMoreView(MethodView):
	roles_accepted('admin','director')
	def get(self,page):
		from_page = 20
		to_page = 40
		kids = Kid.query.filter(Kid.school == g.current_school)
		kids = kids.order_by(Kid.last_name)[from_page:to_page]
		return render_template('back/more_kids.html',kids=kids,more=int(page)+1)


class KidsSearchView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		import json
		from elasticsearch import Elasticsearch

		if 'search' in request.form:
			query = request.form['search'].lower()
			es = Elasticsearch()
			# ELASTIC SEARCHING
			#{"query":{"filtered":{"query":{"multi_match":{"query":"jeffrey","fields":["first_name","last_name"]}},"filter":{"term":{"school_id":"1"}}}}}
			result = es.search(index="faycare",doc_type='kid' , body={"query":{"filtered":{"query":{"multi_match":{"query":query,"fields":["first_name","last_name"]}},"filter":{"term":{"school_id": g.current_school.id }}}}})
			a = []
			for hit in result['hits']['hits']:
				a.append({"title" : "%s %s" % (hit['_source']['first_name'],hit['_source']['last_name']),"url":'kids/%s' % hit['_id']})
			return json.dumps({'results':a})

class BillsActivateView(MethodView):
	@roles_accepted('admin')
	def post(self,bill_id):
		bill = Bill.query.filter_by(id=bill_id,school=g.current_school).first()
		if bill:
			bill.active = True

			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			new_bill = render_template('back/email/new_bill.html',
				kid_name=bill.kid.first_name)

			full_name = "Parent"
			subject = '[%s] You have a new bill!' % g.current_school.name
			tags = ['bills']

			message = get_message_obj_multi(
				new_bill,
				subject,
				bill.kid.parents,
				tags
				)

			if not g.testing:
				m.messages.send(message)

			db.session.commit()
			return jsonify({'result':'OK'});
		else:
			return jsonify({'result':'NO_GOOD'})

class BillsSingleView(MethodView):
	@roles_accepted('admin','parent','director')
	def get(self,kid_id=None,month=None,year=None):
		import calendar
		if month and year and kid_id:
			kid = Kid.query.filter_by(id=kid_id,school=g.current_school).first()
			if not kid:
				return render_template('back/no_bill.html')
			bills = Bill.query.filter(Bill.month == month,
				Bill.year == year,
				Bill.kid == kid,
				Bill.school == g.current_school,
				Bill.active == True).all()

			try:
				current_month = calendar.month_name[int(month)]
			except:
				return render_template('back/no_bill.html',kid=kid)

			if not bills:
				return render_template('back/no_bill.html',kid=kid)
			else:
				total = 0
				for bill in bills:
					total = total + bill.amount
					if bill.credits:
						for credit in bill.credits:
							total = total - credit.amount

			current_invoice = Invoice.query.filter(
				Invoice.kid==kid,
				Invoice.month==month,
				Invoice.year==year
				).first()
			return render_template('back/single_bill.html',bills=bills,
				total=total,
				current_month=current_month,
				current_invoice=current_invoice,
				kid=kid,
				customer_new_parent_form=CustomerNewParentForm())



class BillsChargeCard(MethodView):
	@roles_accepted('admin','parent')
	def post(self,kid_id):
		import stripe

		kid = Kid.query.filter(Kid.id==kid_id,Kid.school==g.current_school).first()
		if not kid:
			flash('That child is not in our system. Your charge did not go through.')
			return redirect(url_for('back.home'))

		charge_amount = int(float(request.form['amount']) * 100)
		stripe.api_key = g.current_school.company.access_token
		if not current_user.customer:
			created_customer = stripe.Customer.create(
				description="Customer at %s" % g.current_school.name,
				email=current_user.email,
				card=request.form['stripeToken']
			)

			credit_card = CreditCard(uuid=created_customer.cards.data[0].id,
				last4=created_customer.cards.data[0].last4,
				exp_month=created_customer.cards.data[0].exp_month,
				exp_year=created_customer.cards.data[0].exp_year,
				type=created_customer.cards.data[0].type,
				)

			customer = Customer(uuid=created_customer.id,
				user=current_user
				)

			customer.credit_cards.append(credit_card)

			db.session.add(customer)
			db.session.add(credit_card)
			db.session.commit()

		charge = stripe.Charge.create(
			amount=charge_amount,
			currency="usd",
			application_fee=30,
			customer=current_user.customer.uuid,
			description="Charging for monthly bill"
		)
		charged_amount = charge.amount / 100
		new_charge = Charge(
			amount=charged_amount,
			time=datetime.datetime.utcnow(),
			kid=kid
		)
		db.session.add(new_charge)

		def recursivly_remove_invoices(charged_amount,new_charge):
			oldest_unpaid_invoice = Invoice.query.filter(Invoice.remaining>0,Invoice.kid==kid).order_by(Invoice.id.desc()).first()
			if not oldest_unpaid_invoice:
				credit_type = CreditType.query.filter_by(name='balance').first()
				balance_credit = Credit(
					amount=charged_amount,
					time=datetime.datetime.utcnow(),
					month=datetime.datetime.utcnow().month,
					year=datetime.datetime.utcnow().year,
					description='Credit applied to balance: %s' % charged_amount,
					status=0,
					credit_type=credit_type,
					school=g.current_school,
					kid=kid)
				db.session.add(balance_credit)

			if oldest_unpaid_invoice.remaining < charged_amount:
				charged_amount = charged_amount - oldest_unpaid_invoice.remaining
				oldest_unpaid_invoice.remaining = 0
				oldest_unpaid_invoice.paid = total
				oldest_unpaid_invoice.charge = new_charge
				db.session.commit()
				oldest_unpaid_invoice = Invoice.query.filter(Invoice.remaining>0,Invoice.kid==kid).order_by(Invoice.id.desc()).first()
				if oldest_unpaid_invoice:
					recursivly_remove_invoices(charged_amount,new_charge)
				elif charged_amount > 0 and not oldest_unpaid_invoice:
					credit_type = CreditType.query.filter_by(name='balance').first()
					balance_credit = Credit(
						amount=charged_amount,
						time=datetime.datetime.utcnow(),
						month=datetime.datetime.utcnow().month,
						year=datetime.datetime.utcnow().year,
						description='Credit applied to balance: %s' % charged_amount,
						status=0,
						credit_type=credit_type,
						school=g.current_school,
						kid=kid)
					db.session.add(balance_credit)
			else:
				oldest_unpaid_invoice.paid = charged_amount
				oldest_unpaid_invoice.remaining = oldest_unpaid_invoice.remaining - charged_amount
				oldest_unpaid_invoice.charge = new_charge

		recursivly_remove_invoices(charged_amount,new_charge)

		db.session.commit()
		flash('Your charge was successful.')

		return redirect(url_for('back.home'))

class BillsView(MethodView):
	@roles_accepted('admin')
	def get(self,kid_id=None):
		bills = None
		custom_bill_form = None
		if g.current_school.company.access_token:
			custom_bill_form = CustomBillForm()
			if kid_id:
				kid = Kid.query.filter(Kid.id == kid_id, Kid.school == g.current_school).first()
				if kid:
					bills = Bill.query.filter(Bill.school == g.current_school,Bill.kid == kid).all()
				else:
					return redirect(url_for('back.bills'))
			else:
				bills = Bill.query.filter(Bill.school == g.current_school).all()
		return render_template('back/bills.html',
			groups=Group.query.filter(Group.school == g.current_school).all(),
			bills=bills,
			bill_by_kid_form=BillByKidForm(),
			custom_bill_form=custom_bill_form,
			kids=Kid.query.filter(Kid.school == g.current_school))

	@roles_accepted('admin')
	def post(self,kid_id=None,group_id=None):
		bill_by_kid_form = None
		custom_bill_form = None
		if 'bill_kid' in request.form:
			bill_by_kid_form = BillByKidForm(request.form)

			if bill_by_kid_form.validate():
				return redirect(url_for('back.bills'))

		elif 'custom_bill_kid' in request.form:
			custom_bill_form = CustomBillForm(request.form)
			if custom_bill_form.validate():
				bill = Bill(
					time=datetime.datetime.utcnow(),
					per_day = 0,
					amount = custom_bill_form.amount.data,
					month=datetime.datetime.utcnow().month,
					year=datetime.datetime.utcnow().year,
					description = custom_bill_form.description.data,
					school = g.current_school,
					status = 0,
					kid = custom_bill_form.custom_bill_kid.data)
				db.session.add(bill)
				db.session.commit()
				return redirect(url_for('back.bills'))

		bills = Bill.query.filter(Bill.school == g.current_school).all()
		for b in bills:
			b.display_time = format_datetime(b.time)
		if not bill_by_kid_form: bill_by_kid_form = BillByKidForm()
		if not custom_bill_form: custom_bill_form = CustomBillForm()
		return render_template('back/bills.html',
			groups=Group.query.filter(Group.school == g.current_school).all(),
			bills=bills,
			bill_by_kid_form=bill_by_kid_form,
			custom_bill_form=custom_bill_form)

class CreditsView(MethodView):
	@roles_accepted('admin')
	def post(self,invoice_id,bill_id):
		invoice = Invoice.query.get(invoice_id)
		bill = Bill.query.filter(Bill.id==bill_id,Bill.school==g.current_school).first()
		if invoice and invoice.bills[0].school == g.current_school and bill:
			credit = Credit()
			dt = datetime.datetime.utcnow()
			dt = dt.replace(tzinfo=pytz.utc)
			credit_form = CreditForm(request.form)
			credit.amount = float(credit_form.amount.data)
			credit.description = credit_form.description.data
			credit.invoice = invoice
			credit.school = g.current_school
			credit.year = dt.year
			credit.time = dt
			credit.month = dt.month
			credit.status = 0
			ct = CreditType.query.filter(CreditType.name=='balance').first()
			credit.credit_type = ct
			credit.kid = bill.kid
			db.session.add(credit)
			db.session.commit()
			return jsonify({'result':'OKAY','remaining':invoice.remaining})
		else:
			return jsonify({'result':'NOGOOD'})


class BillsEditView(MethodView):
	@roles_accepted('admin')
	def get(self,bill_id):
		bill = Bill.query.filter_by(id=bill_id,school=g.current_school).first()
		if bill:
			bill.amount = format_price(amount=bill.amount,currency='')
			bill.per_day = format_price(amount=bill.per_day,currency='')
			bill.month = months_list[bill.month][1]
			return render_template('back/bills_edit.html',
				bill=bill,
				bill_edit_form=BillEditForm(obj=bill),
				credit_form=CreditForm()
			)
		else:
			flash('That bill cannot be found.')
			return redirect(url_for('back.bills'))

	@roles_accepted('admin')
	def post(self,bill_id):
		bill = Bill.query.filter_by(id=bill_id,school=g.current_school).first()
		if bill:
			# print json.loads(request.data)
			bill_edit_form = BillEditForm(request.form,obj=bill)
			bill_edit_form.populate_obj(bill)

			db.session.commit()
			return jsonify({'result':'OKAY'})
		else:
			return jsonify({'result':'NOGOOD'})


class CustomerReceiptsView(MethodView):
	@roles_accepted('admin')
	def get(self):
		import stripe
		stripe.api_key = faycare.apikeys.stripe_api_secret
		all_invoices = stripe.Invoice.all(
			customer=current_user.customer.uuid,
			count=3)
		return jsonify(all_invoices)

class CustomerUpdateView(MethodView):
	@roles_accepted('admin')
	def post(self):
		import stripe
		if 'plan' in request.form:
			original_plan = g.current_school.company.plan
			plan = Plan.query.get(request.form['plan'])
			if plan:
				stripe.api_key = faycare.apikeys.stripe_api_secret
				c = stripe.Customer.retrieve(current_user.customer.uuid)
				updated_subscription = c.update_subscription(plan=plan.name, prorate="True")
				if updated_subscription.plan.id == plan.name:
					# update users plan
					g.current_school.company.plan = plan
					# update subscription
					current_user.customer.subscription.current_period_start = datetime.datetime.fromtimestamp(updated_subscription.current_period_start),
					current_user.customer.subscription.current_period_end = datetime.datetime.fromtimestamp(updated_subscription.current_period_end),
					db.session.commit()
					flash('Your account has been changed. You will receive an email with those changes. We prorate switching plans during a billing cycle.')

					m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

					upgrade_email = render_template('back/email/upgrade.html',
						first_name=current_user.person.first_name)

					full_name = "%s %s" % (current_user.person.first_name,current_user.person.last_name)
					subject = '[Go Tot Go] Nice... You upgraded!'
					tags = ['upgrade']

					message = get_message_obj(
						upgrade_email,
						subject,
						current_user.email,
						full_name,
						tags
						)


					if not g.testing:
						m.messages.send(message)

				else:
					updated_subscription = c.update_subscription(plan=original_plan, prorate="True")
					flash('Something went wrong with your upgrade. We are working on fixing it. Thanks for your patience!')
				return redirect(url_for('back.home'));
			else:
				flash('You trying to hack the sYsTeMz?')
		else:
			flash('Something went wrong with your upgrade. We are working on fixing it. Thanks for your patience.')
		return redirect(url_for('back.home'));

class CustomerNewView(MethodView):
	@roles_accepted('admin')
	def post(self):
		import stripe

		stripe.api_key = faycare.apikeys.stripe_api_secret

		plan = Plan.query.get(request.form['plan'])
		if plan:
			created_customer = stripe.Customer.create(
				description='%s signed up for plan %s for the first time.' % (current_user.email, request.form['plan']),
				card=request.form['stripeToken'],
				email=current_user.email,
				plan=plan.name
			)

			credit_card = CreditCard(uuid=created_customer.cards.data[0].id,
				last4=created_customer.cards.data[0].last4,
				exp_month=created_customer.cards.data[0].exp_month,
				exp_year=created_customer.cards.data[0].exp_year,
				type=created_customer.cards.data[0].type,
				)

			g.current_school.company.plan = Plan.query.filter_by(name=created_customer.subscription.plan.id).first()

			customer = Customer(uuid=created_customer.id,
				user=current_user
				)

			subscription = Subscription(uuid=created_customer.subscription.id,
				start=datetime.datetime.fromtimestamp(created_customer.subscription.start),
				current_period_start=datetime.datetime.fromtimestamp(created_customer.subscription.current_period_start),
				current_period_end=datetime.datetime.fromtimestamp(created_customer.subscription.current_period_end),
				customer=customer
				)

			customer.credit_cards.append(credit_card)

			db.session.add(customer)
			db.session.add(credit_card)
			db.session.add(subscription)
			db.session.commit()

			flash('Your payment went through successfully. Congrats on upgrading. Your account has been upgraded. You should get an email with more information.')
		else:
			flash('Your request was not quite right. ;-)')
		return redirect(url_for('back.home'))

class ParentsEditView(MethodView):
	@roles_accepted('admin','director')
	def get(self,parent_id):
		parent = User.query.filter(User.id==parent_id,User.schools.contains(g.current_school)).first()
		if not parent:
			abort(404)
			return 'NOGOOD'
		parents_form = ParentForm(obj=parent)
		return render_template('back/parent_edit.html',parents_form=parents_form,parent=parent)

	@roles_accepted('admin','director')
	def post(self,parent_id):
		parent = User.query.filter(User.id==parent_id,User.schools.contains(g.current_school)).first()
		parents_form = ParentForm(request.form)

		# if parents_form.validate():
		parent.person.first_name = parents_form.first_name.data
		parent.person.middle_name = parents_form.middle_name.data
		parent.person.last_name = parents_form.last_name.data
		parent.person.cell_phone = parents_form.cell_phone.data
		parent.email = parents_form.email.data
		parent.kids = parents_form.kids.data
		parent.code = str(parents_form.code.data)
		db.session.commit()
		return redirect(url_for('back.parents'))
		# else:
			# return render_template('back/parent_edit.html',parents_form=parents_form,parent=parent,errors=True)

class ParentsView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		parent_form = ParentForm()
		rejection_form = RejectionForm()
		return render_template('back/parents.html',
			rejection_form=rejection_form,
			parents=get_parents(),
			parent_form=parent_form)

	@roles_accepted('admin','director')
	def post(self):
		if request.json:
			parent_form = ParentForm.from_json(request.json)
		else:
			parent_form = ParentForm(request.form)

		if parent_form.validate():
			temp_password = sample_string()
			u = user_datastore.create_user(
				email=parent_form.email.data,
				password=encrypt_password(temp_password),
				kids=parent_form.kids.data)
			u.person = Person(
				first_name=parent_form.first_name.data,
				middle_name=parent_form.middle_name.data,
				last_name=parent_form.last_name.data)
			u.status = 0
			u.pending=False
			dt = datetime.datetime.utcnow()
			dt = dt.replace(tzinfo=pytz.utc)
			u.status_time = dt
			user_datastore.add_role_to_user(u,
				user_datastore.find_role('parent'))
			g.current_school.users.append(u)
			db.session.commit()
			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			welcome_parent_template = render_template('back/email/parent_welcome.html',
				email=parent_form.email.data,
				first_name=parent_form.first_name.data,
				temp_password=temp_password)

			full_name = "%s %s" % (parent_form.first_name.data,parent_form.last_name.data)
			subject = '[%s] Your new account is ready!' % g.current_school.name
			tags = ['login','parents']

			message = get_message_obj(
				welcome_parent_template,
				subject,
				parent_form.email.data,
				full_name,
				tags
				)


			if not g.testing:
				m.messages.send(message)

			flash('Parent saved. Email was sent to %s with a temporary password.' % parent_form.email.data)
			return redirect('/%s/parents/' % g.current_school.u_name)
		return render_template('back/parents.html',
			parents=get_parents(), rejection_form=RejectionForm(),
			parent_form=parent_form,errors=True)

	@roles_accepted('admin','director')
	def delete(self,parent_id):
		parent = User.query.get(parent_id)
		if parent:
			db.session.delete(parent)
			db.session.commit()
		return 'OK'

class AttendanceLoginView(MethodView):
	@roles_accepted('admin','attendance','director','teacher')
	def get(self):
		if g.is_admin or g.is_teacher:
			logout_user();
			attendance_user = get_attendance()[0]
			login_user(attendance_user)
			return redirect('/%s/attendance/' % g.current_school.u_name)

class AttendanceLogView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,staff_id=None,log_date=None):
		who = ''
		is_staff = True
		if staff_id:
			staff = User.query.get(staff_id)
			if not log_date:
				today = datetime.datetime.today()
			else:
				today = datetime.datetime.strptime(log_date,'%m-%d-%Y')
			today = today - datetime.timedelta(hours=today.hour,minutes=today.minute,seconds=today.second,microseconds=today.microsecond)
			tomorrow = today + datetime.timedelta(days=1)
			yesterday = today - datetime.timedelta(days=1)
			who = staff.person.first_name + ' ' + staff.person.last_name
			attendance = Attendance.query.filter(and_(Attendance.school == g.current_school,
				Attendance.user == staff,
				Attendance.attendance_type == 1,
				Attendance.time >= today,
				Attendance.time <= tomorrow)).order_by(asc(Attendance.time)).all()

		else:
			is_staff = False
			who = 'Whole School'
			if not log_date:
				today = datetime.datetime.today()
			else:
				today = datetime.datetime.strptime(log_date,'%m-%d-%Y')
			today = today - datetime.timedelta(hours=today.hour,minutes=today.minute,seconds=today.second,microseconds=today.microsecond)
			tomorrow = today + datetime.timedelta(days=1)
			yesterday = today - datetime.timedelta(days=1)
			attendance = Attendance.query.filter(and_(Attendance.school == g.current_school,
				Attendance.time >= today,
				Attendance.time <= tomorrow)).order_by(asc(Attendance.time)).all()
		
		return render_template('back/attendance_log.html',
			attendance_log=attendance,
			who=who,
			today=today,
			tomorrow=tomorrow,
			yesterday=yesterday,
			is_staff=is_staff,
			staff_id=staff_id)

class AttendanceView(MethodView):
	@roles_accepted('admin','attendance','director','teacher')
	def get(self):
		attendance_form = AttendanceForm()
		secret_user = ''
		if g.is_admin:
			secret_user = get_attendance()[0]
		return render_template('back/attendance.html',
			attendance_form=attendance_form,
			secret_user=secret_user)

	@roles_accepted('admin','attendance','director','teacher')
	def post(self):
		import urllib2
		if 'code' in request.form:
			code = request.form['code']
			user = User.query.filter(
				User.schools.contains(
					g.current_school
				)
			).filter(User.code==code).first()
			
			if user:
				kid_obj = []
				for kid in user.kids:
					kid_obj.append({'id':kid.id,'name':'%s %s' % (kid.first_name,kid.last_name),'hold':kid.hold})

				if user.is_parent():
					user_obj = {'id':user.id,
					'role':'parent',
					'name':'%s %s' % (user.person.first_name,user.person.last_name),
					'kids':kid_obj,
					'status':'OK'}
				elif g.is_teacher or g.is_admin or g.is_director or g.is_attendance:
					user_obj = {'id':user.id,
					'kids':kid_obj,
					'role':'staff',
					'name':'%s %s' % (user.person.first_name,user.person.last_name),
					'status':'OK'}
				else:
					user_obj = {'status':'EMPTY'}
			else:
				user_obj = {'status':'EMPTY'}
			return jsonify(user_obj)
		elif 'kid' in request.form:
			if 'kid' in request.form and 'parent' in request.form and 'status' in request.form:
				parent = User.query.get(request.form['parent'])
				kid = Kid.query.get(request.form['kid'])
				if int(request.form['status']) == int(kid.status):
					if kid.status == 1:
						return jsonify({'status':'NOGOOD','message':'%s is already logged in.' % kid.first_name})
					else:
						return jsonify({'status':'NOGOOD','message':'%s is already logged out.' % kid.first_name})
				status = int(request.form['status'])
				attendance = Attendance()
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				attendance.time = dt
				attendance.school = g.current_school
				attendance.status = status
				attendance.kid = kid
				attendance.user = parent
				attendance.attendance_type = 0
				kid.status = status
				kid.status_time = dt


				get_late_fees(kid,dt)


				db.session.commit()
				# Call socket
				# urllib2.urlopen('http://localhost:3000/info/').read()

			return jsonify({'status':'OK'})
		elif 'kids' in request.form and (g.is_admin or g.is_director):
			if request.form['kids'] == 'all':
				kids = g.current_school.kids.all()
			for kid in kids:
				parent = current_user
				status = int(request.form['status'])
				print status
				attendance = Attendance()
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				attendance.time = dt
				attendance.school = g.current_school
				attendance.status = status
				attendance.kid = kid
				attendance.user = parent
				attendance.attendance_type = 0
				kid.status = status
				kid.status_time = dt
				get_late_fees(kid,dt)

				db.session.add(attendance)

			db.session.commit()
			return jsonify({'status':'OK'})
		elif 'staff' in request.form:
			if 'status' in request.form:
				staff = User.query.get(request.form['staff'])
				status = int(request.form['status'])
				attendance = Attendance()
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				attendance.time = dt
				attendance.school = g.current_school
				attendance.status = status
				attendance.user = staff
				attendance.attendance_type = 1
				staff.status = status
				staff.status_time = dt
				db.session.commit()
				#send socket message
				#urllib2.urlopen('%s?id=attendance&value=700' % config['SOCKET_API']).read()
			return jsonify({'status':'OK'})

class StaffView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self):
		staff_form = StaffForm()
		return render_template('back/staff.html',
			staff=get_staff(),
			staff_form=staff_form)

	@roles_accepted('admin','director')
	def post(self):
		staff_form = StaffForm(request.form)
		if staff_form.validate():
			temp_password = sample_string()
			u = user_datastore.create_user(
				email=staff_form.email.data,
				password=encrypt_password(temp_password))
			u.person = Person(
				first_name=staff_form.first_name.data,
				middle_name=staff_form.middle_name.data,
				last_name=staff_form.last_name.data)
			u.groups = staff_form.groups.data
			if staff_form.is_director.data:
				user_datastore.add_role_to_user(u,
					user_datastore.find_role('director'))
			else:
				user_datastore.add_role_to_user(u,
					user_datastore.find_role('teacher'))
			g.current_school.users.append(u)
			db.session.commit()
			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			welcome_staff_template = render_template('back/email/staff_welcome.html',
				email=staff_form.email.data,
				first_name=staff_form.first_name.data,
				temp_password=temp_password)

			full_name = "%s %s" % (staff_form.first_name.data,staff_form.last_name.data)

			subject = '[%s] Your new account is ready!' % g.current_school.name
			tags = ['login','parents']

			message = get_message_obj(
				welcome_staff_template,
				subject,
				staff_form.email.data,
				full_name,
				tags
				)

			if not g.testing:
				m.messages.send(message)
			else:
				flash('Staff saved. Email was sent to %s with a temporary password.' % staff_form.email.data)
			return redirect('/%s/staff/' % g.current_school.u_name)
		return render_template('back/staff.html',
			staff=get_staff(),
			staff_form=staff_form,errors=True)

class StaffEditView(MethodView):
	@roles_accepted('admin','director')
	def get(self,staff_id):
		staff = User.query.get(staff_id)
		staff_form = StaffEditForm(obj=staff)
		return render_template('back/staff_edit.html',
			staff_form=staff_form,
			staff=staff)

	@roles_accepted('admin','director')
	def post(self,staff_id):
		staff = User.query.get(staff_id)
		old_pending = staff.pending
		staff_form = StaffEditForm(request.form,obj=staff)
		if staff_form.validate():
			staff_form.populate_obj(staff)
			db.session.commit()
			if staff.pending != old_pending and not staff.pending:
				m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

				acceptance_template = render_template('back/email/acceptance.html',
					school_name=g.current_school.name,
					rejection_email='gototgoapp@gmail.com')

				full_name = "Staff"
				subject = '[%s] has accepted your request! Nice!' % g.current_school.name
				tags = ['login','staff','acceptance']

				message = get_message_obj(
					acceptance_template,
					subject,
					staff.email,
					full_name,
					tags
					)

				# turn off email for now
				if not g.testing:
					m.messages.send(message)
			flash('Staff updated successfully.')
			return redirect('/%s/staff/' % g.current_school.u_name)
		else:
			return render_template('back/staff_edit.html',
			staff_form=staff_form,
			staff=staff)

class StaffShowView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,staff_id):

		staff = User.query.filter(
				User.id == staff_id,
				User.schools.contains(
					g.current_school
				)
			).first()
		if staff:
			return render_template('back/staff_show.html',staff=staff)
		else:
			return redirect(url_for('back.home'))

	@roles_accepted('admin','director')
	def delete(self,staff_id):
		staff = User.query.get(staff_id)
		if staff:
			db.session.delete(staff)
			db.session.commit()
		return 'OK'

class MedicalView(MethodView):
	@roles_accepted('admin','director')
	def get(self):

		return render_template('back/medical.html',
			allergen_form=AllergenForm(),
			vaccination_form=VaccineForm())

class AllergensView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		allergen_form = AllergenForm(request.form)
		if allergen_form.validate():
			allergen = Allergen(name=allergen_form.name.data,school=g.current_school)
			db.session.add(allergen)
			db.session.commit()
			return redirect(url_for('back.medical'))
		return render_template('back/medical.html',allergen_form=allergen_form)

	@roles_accepted('admin','director')
	def delete(self,allergen_id):
		allergen = Allergen.query.filter(Allergen.id==allergen_id,Allergen.school==g.current_school).first()
		if not allergen:
			return jsonify({'result':'NO_GOOD'})
		else:
			db.session.delete(allergen)
			db.session.commit()
			return jsonify({'result':'OKAY'})

class GroupsEditView(MethodView):
	@roles_accepted('admin','director')
	def get(self,group_id):
		group = Group.query.get(group_id)
		groups_form = GroupsForm(obj=group)
		return render_template('back/group_edit.html',
			group=group,
			groups_form=groups_form)

	@roles_accepted('admin','director')
	def post(self,group_id):
		group = Group.query.get(group_id)
		groups_form = GroupsForm(request.form, obj=group)
		if groups_form.validate():
			groups_form.populate_obj(group)
			db.session.commit()
			return redirect('/%s/groups/' % g.current_school.u_name)
		else:
			return render_template('back/group_edit.html',
				group=group,
				groups_form=groups_form)

class GroupsShowView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,group_id):
		group = Group.query.get(group_id)
		kids_in = group.active_kids.filter(Kid.status == 1).count()
		kids_out = group.active_kids.filter(Kid.status == 0).count()
		return render_template('back/group_show.html',
			group=group,
			kids_in=kids_in,
			kids_out=kids_out)

	@roles_accepted('admin','director')
	def delete(self,group_id):
		group = Group.query.get(group_id)
		if group:
			db.session.delete(group)
			db.session.commit()
			return jsonify({'result':'OK'})

class AgeRangeView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		age_range_form = AgeRangeForm(request.form)
		if age_range_form.validate():
			age_range = AgeRange(
				start_age=age_range_form.start_age.data,
				end_age=age_range_form.end_age.data,
				needs_potty_trained=age_range_form.needs_potty_trained.data,
				school=g.current_school
			)
			db.session.add(age_range)
			db.session.commit()
			return redirect(url_for('back.groups'))
		else:
			return render_template('back/age_range.html',age_range_form=age_range_form)

class GroupsView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self):
		groups_form = GroupsForm()
		age_range_form = AgeRangeForm()
		groups = g.current_school.groups
		age_ranges = g.current_school.age_ranges
		return render_template('back/groups.html',
			groups_form=groups_form,
			age_range_form=age_range_form,
			age_ranges=age_ranges,
			groups=groups)

	@roles_accepted('admin','director')
	def post(self):
		groups_form = GroupsForm(request.form)
		if groups_form.validate():
			group = Group()
			group.name = groups_form.name.data
			group.kids = groups_form.kids.data
			for kid in groups_form.kids.data:
				kid.current_group = groups_form.name.data
			group.school = g.current_school
			group.staff = groups_form.staff.data

			for between_hours in BetweenSchedule.query.all():
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
				between_hours.prices.append(p)

			for calendar_hours in CalendarSchedule.query.all():
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
				calendar_hours.prices.append(p)

			for hourly in HourlySchedule.query.all():
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
				hourly.prices.append(p)

			for one_for_all in OneForAllSchedule.query.all():
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
				one_for_all.prices.append(p)

			db.session.commit()
			flash('Group saved!')
			return redirect('/%s/groups/' % g.current_school.u_name)
		return render_template('back/groups.html',
			groups_form=groups_form)

class GroupsKidsView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self,group_id):
		import json
		group = Group.query.get(group_id)
		return json.dumps([kid.serialize for kid in group.kids.order_by().all()])

class SchedulesView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		return render_template('back/schedules.html',billing_units=billing_units_display)

class SchoolHoursReset(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		g.current_school.school_hours = None
		db.session.commit()
		return jsonify({'result':'OK'});

class SchedulesDeleteView(MethodView):
	@roles_accepted('admin','director')
	def delete(self,schedule_id):
		schedule = BetweenSchedule.query.get(schedule_id)
		db.session.delete(schedule)
		db.session.commit()
		return 'OK DELETE'

class SchedulesDaysView(MethodView):
	@roles_accepted('admin','director')
	def get(self, schedule_id, schedule_type):
		schedule = get_schedule(schedule_id, schedule_type)

		if schedule:
			schedule_days_form = BetweenHoursForm(obj=schedule)
			set_form_per_school_hours(schedule_days_form)
			return render_template('back/schedules_edit_days.html',
				schedule=schedule,
				schedule_type=schedule_type,
				schedule_days_form=schedule_days_form)
		else:
			return redirect(url_for('back.home'))

	@roles_accepted('admin','director')
	def post(self, schedule_id, schedule_type):
		schedule = get_schedule(schedule_id, schedule_type)
		if schedule:
			schedule_days_form = BetweenHoursForm(request.form)
			if schedule_days_form.validate():
				format = '%H:%M:%S'
				schedule.name = schedule_days_form.name.data

				for day in days:
					setattr(schedule,day + '_start', datetime.datetime.strptime(getattr(schedule_days_form,day + '_start').data,format).time())
					setattr(schedule,day + '_end', datetime.datetime.strptime(getattr(schedule_days_form,day + '_start').data,format).time())

				db.session.add(schedule)
				db.session.commit()
				return redirect(url_for('back.schedules'))
			else:
				return render_template('back/schedules_edit_days.html',
				schedule=schedule,
				schedule_type=schedule_type,
				schedule_days_form=schedule_days_form)

class SchedulesActivateView(MethodView):
	@roles_accepted('admin','director')
	def post(self,schedule_type,schedule_id):
		schedule = get_schedule(schedule_id,schedule_type)
		if schedule:
			if not schedule.active:
				schedule.active = True
				if g.current_school.billing_on:
					for kid in schedule.kids:
						money_helpers.create_bill(kid)

				db.session.commit()


		return jsonify({'result':'OK'})


class SchedulesNewKidsView(MethodView):
	@roles_accepted('admin','director')
	def get(self, schedule_id, schedule_type):
		schedule = get_schedule(schedule_id, schedule_type)

		if schedule:
			schedule_kids_form = ScheduleKidsForm(obj=schedule)
			return render_template('back/schedules_add_kids.html',
				schedule=schedule,
				schedule_type=schedule_type,
				schedule_kids_form=schedule_kids_form)
		else:
			return redirect(url_for('back.home'))

	@roles_accepted('admin','director')
	def post(self,schedule_id,schedule_type):
		schedule = get_schedule(schedule_id, schedule_type)

		if schedule:
			schedule_kids_form = ScheduleKidsForm(request.form)
			if schedule_kids_form.validate():
				needs_billing = []
				if schedule.active:
					for kid in schedule_kids_form.kids.data:
						kid_schedule = money_helpers.get_schedule_for_kid(kid)
						if schedule != kid_schedule:
							needs_billing.append(kid)
				schedule.kids = schedule_kids_form.kids.data
				db.session.commit()
				if g.current_school.billing_on:
					for kid in needs_billing:
						money_helpers.prorate_and_bill(kid,force_billing=True)
				return redirect(url_for('back.schedules'))
			return render_template('back/schedules_add_kids.html',
				schedule=schedule,
				schedule_type=schedule_type,
				schedule_kids_form=schedule_kids_form)
		else:
			return redirect(url_for('back.home'))

class SchedulesPricesView(MethodView):
	@roles_accepted('admin','director')
	def get(self, schedule_id, schedule_type):
		schedule = get_schedule(schedule_id, schedule_type)

		if schedule:
			return render_template('back/schedules_edit_prices.html',schedule=schedule, billing_units=billing_units_display)
		else:
			return redirect(url_for('back.home'))

class SchedulesNewView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		between_hours_form = BetweenHoursForm()
		set_form_per_school_hours(between_hours_form)
		one_for_all_form = OneForAllForm()
		hourly_form = HourlyForm()
		calendar_form = CalendarForm()
		return render_template('back/schedules_new.html',
			between_hours_form=between_hours_form,
			one_for_all_form=one_for_all_form,
			hourly_form=hourly_form,
			calendar_form=calendar_form)

class SchedulesSchoolHoursView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		school_hours_form = SchoolHoursForm()
		return render_template('back/schedules_school_hours_new.html',
			school_hours_form=school_hours_form )

	@roles_accepted('admin','director')
	def post(self):
		school_hours_form = SchoolHoursForm(request.form)
		school_hours = SchoolHours()
		format = '%H:%M:%S'
		for day in days:
			if not getattr(school_hours_form,'closed_' + day).data:
				setattr(school_hours,day + '_start', datetime.datetime.strptime(getattr(school_hours_form,day + '_start').data,format).time())
				setattr(school_hours,day + '_end', datetime.datetime.strptime(getattr(school_hours_form,day + '_end').data,format).time())
			else:
				setattr(school_hours,'closed_' + day, getattr(school_hours_form,'closed_' + day).data)

		school_hours.name = 'SCHOOL_HOURS'

		school_hours.school = g.current_school

		db.session.add(school_hours)
		db.session.commit()
		return redirect(url_for('back.schedules'))

class SchedulesBetweenView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		between_hours_form = BetweenHoursForm(request.form)
		format = '%H:%M:%S'
		between_hours = BetweenSchedule()
		between_hours.name = between_hours_form.name.data

		for day in days:
			if not getattr(between_hours_form,'closed_' + day).data:
				setattr(between_hours,day + '_start', datetime.datetime.strptime(getattr(between_hours_form,day + '_start').data,format).time())
				setattr(between_hours,day + '_end', datetime.datetime.strptime(getattr(between_hours_form,day + '_end').data,format).time())
			else:
				setattr(between_hours,'closed_' + day, getattr(between_hours_form,'closed_' + day).data)

		between_hours.school = g.current_school

		for group in g.current_school.groups:
			p = Price(billing_duration=30,price=5.00,group=group,billing_unit=0)
			between_hours.prices.append(p)

		db.session.add(between_hours)
		db.session.commit()
		return redirect(url_for('back.schedules'))

class SchedulesForAllView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		one_for_all_form = OneForAllForm(request.form)
		if one_for_all_form.validate():
			one_for_all = OneForAllSchedule()
			one_for_all.name = one_for_all_form.name.data
			one_for_all.school = g.current_school

			for group in g.current_school.groups:
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=0)
				one_for_all.prices.append(p)

			db.session.add(one_for_all)
			db.session.commit()
			return redirect(url_for('back.schedules'))
		else:
			return render_template('back/schedules_new.html',one_for_all_form=one_for_all_form)

class SchedulesHourlyView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		hourly_form = HourlyForm(request.form)
		if hourly_form.validate():
			hourly = HourlySchedule()
			hourly.name = hourly_form.name.data
			hourly.school = g.current_school

			for group in g.current_school.groups:
				p = Price(billing_duration=1,price=5.00,group=group,billing_unit=3)
				hourly.prices.append(p)

			db.session.add(hourly)
			db.session.commit()
			return redirect(url_for('back.schedules'))
		else:
			return render_template('back/schedules_new.html',one_for_all_form=one_for_all_form)

class SchedulesCalendarView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		calendar_form = CalendarForm(request.form)
		if calendar_form.validate():
			calendar = CalendarSchedule()
			calendar.name = calendar_form.name.data
			calendar.school = g.current_school

			for group in g.current_school.groups:
				p = Price(billing_duration=30,price=5.00,group=group,billing_unit=0)
				calendar.prices.append(p)

			db.session.add(calendar)
			db.session.commit()
			return redirect(url_for('back.schedules'))
		else:
			return render_template('back/schedules_new.html',one_for_all_form=one_for_all_form)

class SchedulesCalendarApproveView(MethodView):
	@roles_accepted('admin','director')
	def post(self,kid_id):
		from sqlalchemy import extract
		from dateutil.relativedelta import relativedelta
		import json
		kid = Kid.query.filter(Kid.id == kid_id,Kid.school == g.current_school).first()
		to_bill_dates = []
		if kid:
			month = request.form['month']
			year = request.form['year']
			q = CalendarAttendanceDay.query.filter(and_(
				extract('month',CalendarAttendanceDay.date) == int(month),
				extract('year',CalendarAttendanceDay.date) == int(year),
				CalendarAttendanceDay.kid == kid))
			q.delete(synchronize_session='fetch')
			db.session.commit()

			dates = json.loads(request.form['dates'])
			for date in dates:
				date = datetime.datetime.strptime(date,'%m/%d/%Y')
				if date.month == int(month) and date.year == int(year):
					cal_date = CalendarAttendanceDay(
						date=date,
						kid=kid,
						school=g.current_school,
						approved=True
					)
					to_bill_dates.append(cal_date)
					db.session.add(cal_date)

			db.session.commit()

			if to_bill_dates:
				if g.current_school.billing_on:
					money_helpers.create_calendar_bill(kid,to_bill_dates,month,year)

			last_month = datetime.datetime.utcnow() - relativedelta(months=1,day=1)
			two_month_from_now = datetime.datetime.utcnow() + relativedelta(months=2,day=1)
			dates = CalendarAttendanceDay.query.filter(
				CalendarAttendanceDay.kid==kid,
				and_(
				CalendarAttendanceDay.date >= last_month,CalendarAttendanceDay.date <= two_month_from_now )).all()
			json_dates = []
			for date in dates:
				json_dates.append(date.date.strftime('%m/%d/%Y'))

			return json.dumps({'result':'OK','dates':json_dates})
		else:
			return jsonify({'result':'NOGOOD'});

class SchedulesGetCalendarView(MethodView):
	@roles_accepted('admin','director')
	def get(self,kid_id):
		from dateutil.relativedelta import relativedelta
		import json
		kid = Kid.query.filter_by(id=kid_id,school=g.current_school).first()
		last_month = datetime.datetime.utcnow() - relativedelta(months=1,day=1)
		two_month_from_now = datetime.datetime.utcnow() + relativedelta(months=3,day=1)
		if kid:
			dates = CalendarAttendanceDay.query.filter(
				CalendarAttendanceDay.kid==kid,
				and_(
				CalendarAttendanceDay.date >= last_month,CalendarAttendanceDay.date <= two_month_from_now )).all()
			json_dates = []
			locked_months = []
			for date in dates:
				if date.approved:
					if not date.date.month in locked_months:
						locked_months.append(date.date.month)
				json_dates.append(date.date.strftime('%m/%d/%Y'))
			return json.dumps({'dates':json_dates,'locked_months':locked_months})
		else:
			return jsonify({'result':'NOGOOD'})

class SchedulesSetCalendarView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		import json
		from sqlalchemy import extract
		if 'kid_id' in request.form and 'dates' in request.form:
			kid = Kid.query.filter(Kid.id==request.form['kid_id'],Kid.school==g.current_school).first()
			if not kid:
				return jsonify({'result':'NO_GOOD'})

		month = request.form['month']
		year = request.form['year']

		q = CalendarAttendanceDay.query.filter(and_(
			extract('month',CalendarAttendanceDay.date) == int(month),
			extract('year',CalendarAttendanceDay.date) == int(year),
			CalendarAttendanceDay.kid == kid))
		q.delete(synchronize_session='fetch')

		dates = json.loads(request.form['dates'])

		for date in dates:
			new_date = datetime.datetime.strptime(date,'%m/%d/%Y')
			if new_date.month == int(month) and new_date.year == int(year):
				cal_date = CalendarAttendanceDay(
					date=new_date,
					kid=kid,
					school=g.current_school
				)
				db.session.add(cal_date)

		db.session.commit()

		return jsonify({'result':'OK'})


def get_schedule_for_price(price):
	between_schedule = price.query.first().between_schedules.first()
	one_for_all_schedule = price.query.first().one_for_all_schedules.first()
	calendar_schedule = price.query.first().calendar_schedules.first()
	if between_schedule:
		return between_schedule
	elif one_for_all_schedule:
		return one_for_all_schedule
	elif calendar_schedule:
		return calendar_schedule

class PricesEditView(MethodView):
	@roles_accepted('admin','director')
	def get(self,price_id):
		# check that this request came from AJAX
		if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
			price = Price.query.get(price_id)
			if g.current_school.bill_equal:
				schedule_price_form = ScheduleEqualPriceForm(obj=price)
			else:
				schedule_price_form = SchedulePriceForm(obj=price)
			price_schedule = get_schedule_for_price(price)
			return render_template('/back/single_price_edit.html',
				price=price,
				schedule_price_form=schedule_price_form)
		else:
			abort(404)
			return 'NO GOOD'

	@roles_accepted('admin','director')
	def post(self,price_id):
		# check that this request came from AJAX
		if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
			if 'price' in request.form:
				price = Price.query.get(price_id)
				price.price = request.form['price']
				if not g.current_school.bill_equal:
					price.billing_duration = request.form['billing_duration']
					price.billing_unit = int(request.form['billing_unit'])
				else:
					price.billing_duration = 1
					price.billing_unit = 1
				if request.form['late_price'] == '':
					price.late_price = 0.0
				else:
					price.late_price = request.form['late_price']



				db.session.commit()
			return jsonify({ 'price':price.price,'billing_duration':price.billing_duration,'billing_unit':price.billing_unit,'late_price':price.late_price })
		else:
			abort(404)
			return 'NO GOOD'

class ReportsView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		avg_scholarship = db.session.query(func.avg(Kid.scholarship)).filter(Kid.school == g.current_school, Kid.scholarship > 0).first()[0]
		sum_scholarship = db.session.query(func.sum(Kid.scholarship)).filter(Kid.school == g.current_school, Kid.scholarship > 0).first()[0]
		avg_govt = db.session.query(func.avg(Kid.govt)).filter(Kid.school == g.current_school,Kid.govt > 0).first()[0]
		sum_govt = db.session.query(func.sum(Kid.govt)).filter(Kid.school == g.current_school,Kid.govt > 0).first()[0]
		govt_count = Kid.query.filter(Kid.govt > 0).count()
		scholarship_count = Kid.query.filter(Kid.scholarship > 0).count()
		return render_template('/back/reports.html',
			avg_scholarship=avg_scholarship,
			sum_scholarship=sum_scholarship,
			scholarship_count=scholarship_count,
			avg_govt=avg_govt,
			sum_govt=sum_govt,
			attendance_download_form=AttendanceDownloadForm(),
			govt_count=govt_count)

class SettingsView(MethodView):
	def get(self):
		from boto.s3.connection import S3Connection

		# 2 gigs is 2147483648
		bytes_available = float(2147483648)
		personal_info_form = PersonalInfoForm(obj=current_user.person)
		photo_space = '0.0'
		percent_used = '0.0'

		if current_user.bucket:
			conn = S3Connection(faycare.apikeys.AWSAccessKeyId,faycare.apikeys.AWSSecretKey)
			bucket = conn.get_bucket(current_user.bucket)
			total_size = float(0)
			for i in bucket:
				total_size += i.size

			percent_used = str(round((total_size / bytes_available) * float(100),2))
			photo_space = sizeof_fmt(total_size)

		passed_plan = False
		plans = []
		# determine if it is an upgrade or downgrade
		for plan in Plan.query.all():
			plan.upgrade = False
			if passed_plan:
				plan.upgrade = True
			if plan == g.current_school.company.plan:
				passed_plan = True
			plans.append(plan)

		plans.reverse()
		g.current_school.timezone = current_user.timezone
		settings_form = SettingsForm(obj=g.current_school)

		kids_so_far = 0
		for school in current_user.schools:
			kids_so_far = kids_so_far + school.kids.count()

		total_kids_available = g.current_school.company.plan.limit

		return render_template('back/settings.html',
			settings_form=settings_form,
			new_school_form=NewSchoolForm(),
			change_plan_form=ChangePlanForm(),
			customer_new_form=CustomerNewForm(),
			photo_space=photo_space,
			kids_so_far=kids_so_far,
			total_kids_available=total_kids_available,
			percent_used=percent_used,
			plans=plans,
			stripe_pushable=faycare.apikeys.stripe_api_pushable,
			personal_info_form=personal_info_form)

	def post(self):
		if 'first_name' in request.form:
			personal_info_form = PersonalInfoForm(request.form)
			if personal_info_form.validate():
				current_user.person.first_name = personal_info_form.first_name.data
				current_user.person.middle_name = personal_info_form.middle_name.data
				current_user.person.last_name = personal_info_form.last_name.data
				cell_phone = personal_info_form.cell_phone.data.replace(' ','').replace('(','').replace(')','').replace('-','').replace('_','').replace('+','')
				current_user.person.cell_phone = cell_phone
				db.session.commit()
				flash('Your settings were saved!')
				return redirect(url_for('back.home'))
			settings_form = SettingsForm(u_name=g.current_school.u_name,website_on=g.current_school.website_on)
			return render_template('back/settings.html',
				settings_form=settings_form,
				personal_info_form=personal_info_form)
		else:
			settings_form = SettingsForm(request.form)
			if settings_form.validate():
				if settings_form.data['u_name'] != g.current_school.u_name:
					g.current_school.u_name = settings_form.data['u_name']

				g.current_school.searchable = settings_form.searchable.data
				g.current_school.bill_equal = settings_form.bill_equal.data
				g.current_school.billing_on = settings_form.billing_on.data

				current_user.timezone = settings_form.timezone.data
				if settings_form.grace.data == '':
					g.current_school.grace = -1
				else:
					g.current_school.grace = settings_form.grace.data
				db.session.commit()
				flash('Your settings were saved!')
				return redirect('/%s/settings/' % g.current_school.u_name)
			personal_info_form = PersonalInfoForm(obj=current_user.person)
			return render_template('back/settings.html',
				settings_form=settings_form,
				personal_info_form=personal_info_form)

class RejectionView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		if 'parent_id' in request.form and 'reason' in request.form:
			user_in_question = User.query.get(request.form['parent_id'])
			rejection = Rejection(
				reason=request.form['reason'],
				email=user_in_question.email,
				time=datetime.datetime.utcnow()
			)
			g.current_school.users.remove(user_in_question)
			db.session.add(rejection)
			db.session.commit()

			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			rejection_template = render_template('back/email/rejection.html',
				school_name=g.current_school.name,
				reason=request.form['reason'],
				rejection_email='jacob@gototgo.com')

			full_name = "Friend"
			subject = '[%s] has rejected your request!' % g.current_school.name
			tags = ['login','parents','rejection']

			message = get_message_obj(
				rejection_template,
				subject,
				user_in_question.email,
				full_name,
				tags
				)

			# turn off email for now
			if not g.testing:
				m.messages.send(message)

		return 'OK'

class OrderView(MethodView):
	@roles_accepted('admin','director')
	def get(self,order_id=None):
		if not order_id:
			orders = Order.query.filter(Order.school == g.current_school).all()
			open_orders = Order.query.filter(Order.school == g.current_school,Order.status==0).all()
			completed_orders = Order.query.filter(Order.school == g.current_school,Order.status==1).all()
			return render_template('back/orders.html',
				orders=orders,
				open_orders=open_orders,
				completed_orders=completed_orders,
				order_form=OrderForm())
		else:
			order = Order.query.get(order_id)
			if order:
				if order.school:
					if order.school == g.current_school:
						if request.endpoint == 'back.orders_single_edit':
							return render_template('back/orders_single_edit.html',order=order)
						elif request.endpoint == 'back.orders_single':
							return render_template('back/orders_single.html',
								order=order,
								comment_form=CommentForm())
					else:
						abort(404)
						return redirect('/')
				else:
					abort(404)
					return redirect('/')
			else:
				abort(404)
				return redirect('/')


	@roles_accepted('admin','director')
	def post(self,order_id=None):
		if not order_id:
			form = OrderForm(request.form)
			if form.validate():
				new_tags = []
				for tag in form.tags.data.split(','):
					tag_exists = OrderTag.query.filter(OrderTag.name==tag).first()
					if tag_exists:
						new_tags.append(tag_exists)	
					else:
						tag = OrderTag(name=tag)
						db.session.add(tag)
						new_tags.append(tag)
				order = Order(
					title=form.title.data,
					content=form.content.data,
					assignee=form.assignee.data,
					status=0,
					order_type=form.order_type.data,
					priority=form.priority.data,
					created_at=datetime.datetime.now(),
					order_tags=new_tags,
					updated_at=datetime.datetime.now(),
					due_date=datetime.datetime.strptime(form.due_date.data,'%m/%d/%Y'),
					school=g.current_school
				)
				db.session.add(order)
				db.session.commit()
				return redirect(url_for('back.orders'))
			else:
				orders = Order.query.filter(Order.school == g.current_school).all()
				open_orders = Order.query.filter(Order.school == g.current_school,Order.status==0).all()
				completed_orders = Order.query.filter(Order.school == g.current_school,Order.status==1).all()
				return render_template('back/orders.html',
					orders=orders,
					open_orders=open_orders,
					completed_orders=completed_orders,
					order_form=form,
					errors=True)
		else:
			order = Order.query.get(order_id)
			if order:
				if order.school == g.current_school:
					data = request.json
					if 'status' in data:
						order.status = int(data['status'])
						db.session.commit()

						template = render_template('back/email/order_update.html')
						subject = '[%s] Your order has been updated recently.' % g.current_school.name
						tags = ['orders',order.order_type.name.lower()]

						message = get_message_obj(template,subject,order.assignee.email,order.assignee.person.full_name,tags)

						m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)
						if not g.testing:
							m.messages.send(message)

						return jsonify({'result':'OKAY','status':data['status']})
					else:
						return jsonify({'result':'NOGOOD'})
				else:
					return jsonify({'result':'NOGOOD'})
			else:
				return jsonify({'result':'NOGOOD'})


class OrderCommentView(MethodView):
	@roles_accepted('admin','director')
	def post(self,order_id):
		order = Order.query.get(order_id)
		if order:
			if order.school:
				if order.school == g.current_school:
					form = CommentForm.from_json(request.json)
					if form.validate():
						comment = OrderComment(
							order=order,
							created_at=datetime.datetime.now(),
							updated_at=datetime.datetime.now(),
							author=current_user,
							content=form.content.data
						)
						db.session.add(comment)
						db.session.commit()

						template = render_template('back/email/order_update.html',order_url=url_for('back.orders_single',order_id=order.id))
						subject = '[%s] Your order has been updated recently.' % g.current_school.name
						tags = ['orders',order.order_type.name.lower()]

						message = get_message_obj(template,subject,order.assignee.email,order.assignee.person.full_name,tags)

						m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)
						if not g.testing:
							m.messages.send(message)

						return jsonify({'response':'OKAY','comment':comment.serialize})
					else:
						return jsonify({'response':'NOGOOD','errors':form.errors})
				else:
					abort(404)
					return jsonify({'response':'NOGOOD'})
			else:
				abort(404)
				return jsonify({'response':'NOGOOD'})
		else:
			abort(404)
			return jsonify({'response':'NOGOOD'})

class VaccinationView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		vaccination_form = VaccineForm(request.form)
		vaccination = Vaccination(vaccine=vaccination_form.vaccine.data)
		for due_date in vaccination_form.due_dates.data:
			try:
				int(due_date)
			except Exception, e:
				due_date = 0
			due_date = DueDate(months=due_date)
			db.session.add(due_date)
			vaccination.due_dates.append(due_date)
			vaccination.school = g.current_school
		db.session.add(vaccination)
		db.session.commit()

		return redirect(url_for('back.medical'))

	@roles_accepted('admin','director')
	def delete(self,vaccination_id):
		vaccination = Vaccination.query.filter(Vaccination.id==vaccination_id,Vaccination.school==g.current_school).first()
		if not vaccination:
			return jsonify({'result':'NOGOOD'})
		else:
			db.session.delete(vaccination)
			db.session.commit()
			return jsonify({'result':'OKAY'})

class DueDateView(MethodView):
	@roles_accepted('admin','director')
	def post(self,due_date_id):
		if 'new_val' in request.form:
			due_date = DueDate.query.get(due_date_id)
			if due_date:
				due_date.months = request.form['new_val']
				db.session.commit()
				return jsonify({'result':'OK'})
		return jsonify({'result':'NOGOOD'})

class AcceptanceView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		if 'parent_id' in request.form:
			user_in_question = User.query.get(request.form['parent_id'])
			user_in_question.pending = False
			db.session.commit()

			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			acceptance_template = render_template('back/email/acceptance.html',
				school_name=g.current_school.name,
				acceptance_email='heyyou@gototgo.com')

			full_name = "Friend"
			subject = '[%s] has accepted your request!' % g.current_school.name
			tags = ['login','parents','acceptance']

			message = get_message_obj(
				acceptance_template,
				subject,
				user_in_question.email,
				full_name,
				tags
				)

			# turn off email for now
			if not g.testing:
				m.messages.send(message)

		return 'OK'

class SettingsForm(Form):
	u_name = TextField('Unique slug')
	website_on = BooleanField()
	timezone = SelectField(choices=[(t['tz'],t['txt']) for t in timezones])
	searchable = BooleanField()
	billing = BooleanField()
	grace = TextField('Grace period (in minutes)')
	bill_equal = BooleanField('Equal bill each month?')
	billing_on = BooleanField('Use the billing capabilities of this app.')

	def validate(self):
		rv = Form.validate(self)
		if not rv:
			return False
		#validate the uname for uniqueness
		if self.u_name.data != g.current_school.u_name:
			if db.session.query(exists().where(School.u_name == self.u_name.data)).scalar():
				self.u_name.errors.append('Slug already taken')
				return False

		return True

class ActivitiesView(MethodView):
	@roles_accepted('admin','teacher','director')
	def delete(self,activity_id):
		activity = Activity.query.filter(Activity.id==activity_id,Activity.school == g.current_school).first()
		if activity:
			db.session.delete(activity)
			db.session.commit()
			return jsonify({'result':'OK'})
		else:
			return jsonify({'result':'NO GOOD'})

class MessagesView(MethodView):
	@roles_accepted('admin','teacher','director')
	def post(self):
		from twilio.rest import TwilioRestClient
		import json

		if 'json_data' in request.form:
			form_data = json.loads(request.form['json_data'])
			message = None
			if form_data.get('message_who') == 'kid' and form_data.get('message_kid'):
				kid = Kid.query.get(form_data.get('message_kid'))
				if kid and kid.school == g.current_school:
					message = Message(sender=current_user,
					time=datetime.datetime.utcnow(),
					status=0,
					message=form_data.get('message_message')
					)

					for parent in kid.parents:
						message.recipients.append(parent)

					db.session.add(message)
					db.session.commit()

					subject = '[%s] You have a new message!' % g.current_school.name
					tags = ['messages','parents']

			if form_data.get('message_who') == 'staff' and form_data.get('message_staff'):
				staff = User.query.get(form_data.get('message_staff'))
				if staff and staff.schools.filter(User.schools.contains(g.current_school)):
					message = Message(sender=current_user,
					time=datetime.datetime.utcnow(),
					status=0,
					message=form_data.get('message_message')
					)

					message.recipients.append(staff)

					db.session.add(message)
					db.session.commit()

					subject = '[%s] You have a new message!' % g.current_school.name
					tags = ['messages','staff']

			if form_data.get('message_who') == 'parent' and form_data.get('message_parent'):
				parent = User.query.get(form_data.get('message_parent'))
				if parent and parent.schools.filter(User.schools.contains(g.current_school)):
					message = Message(sender=current_user,
					time=datetime.datetime.utcnow(),
					status=0,
					message=form_data.get('message_message')
					)

					message.recipients.append(parent)

					db.session.add(message)
					db.session.commit()

					subject = '[%s] You have a new message!' % g.current_school.name
					tags = ['messages','parent']

			if form_data.get('message_who') == 'school':
				message = Message(sender=current_user,
				time=datetime.datetime.utcnow(),
				status=0,
				message=form_data.get('message_message')
				)
				for user in g.current_school.users:
					if user.person:
						message.recipients.append(user)

				db.session.add(message)
				db.session.commit()

				subject = '[%s] You have a new message!' % g.current_school.name
				tags = ['messages','whole_school']

			if form_data.get('message_who') == 'group':
				message = Message(sender=current_user,
				time=datetime.datetime.utcnow(),
				status=0,
				message=form_data.get('message_message')
				)
				for kid in g.current_school.kids:
					for parent in kid.parents:
						if parent not in message.recipients:
							message.recipients.append(parent)

				db.session.add(message)
				db.session.commit()

				subject = '[%s] You have a new message!' % g.current_school.name
				tags = ['messages','group']


			m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

			message_template = render_template('back/email/message.html',
			person=current_user.person.first_name,
			message=message.message,
			time=format_datetime(message.time),
			school=g.current_school.name)


			for recipient in message.recipients:
				message_mandrill = get_message_obj_multi(
					message_template,
					subject,
					[recipient],
					tags
					)


				if not g.testing:
					m.messages.send(message_mandrill)

			# SEND TEXT
			if not g.testing:
				for user in message.recipients:
					if user.person.cell_phone:
						account_sid = faycare.apikeys.twilio_account_sid
						auth_token  = faycare.apikeys.twilio_auth_token
						client = TwilioRestClient(account_sid, auth_token)
						message_sms = client.sms.messages.create(body=message.message,
						to=user.person.cell_phone,
						from_=faycare.apikeys.twilio_tele)
		return 'OKAY'

class ImportView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		return render_template('back/import.html')

class SchoolsNewView(MethodView):
	@roles_accepted('admin')
	def post(self):
		new_school_form = NewSchoolForm(request.form)
		if new_school_form.validate():
			school = School(name=new_school_form.name.data,
				city=new_school_form.city.data,
				state=new_school_form.state.data,
				u_name=get_u_name(),
				timezone=current_user.timezone,
				company=g.current_school.company,
				searchable=True
			)

			days = ['mon','tues','weds','thurs','fri','sat','sun']
			school_hours = SchoolHours()
			for day in days:
				setattr(school_hours,'%s_start' % day,datetime.time(6,30,00))
				setattr(school_hours,'%s_end' % day,datetime.time(18,30,00))
				if day == 'sat' or day == 'sun':
					setattr(school_hours,'closed_%s' % day,True)
				else:
					setattr(school_hours,'closed_%s' % day,False)
			school_hours.name = '%s school hours' % school.name
			db.session.add(school_hours)
			school_hours.school = school
			attendance_user = user_datastore.create_user(
				email='%s@%s.com' % (school.u_name,faycare.apikeys.app_name),
				password=encrypt_password(sample_string(15)))
			user_datastore.add_role_to_user(
				attendance_user,
				user_datastore.find_role('attendance')
			)
			attendance_user.schools.append(school)

			current_user.schools.append(school)
			db.session.add(school)
			db.session.commit()
			flash('Congrat! You\'ve added a new school!  You can easily toggle between your schools by using the dropdown menu in the top bar or in the pull out menu when using a mobile device.')
			return redirect(url_for('back.home'))


class AttendanceStatusView(MethodView):
	@roles_accepted('admin','teacher','director')
	def get(self):
		import time
		time.sleep(5)
		return jsonify({ 'result':'OK' })

class AttendancePing(MethodView):
	@roles_accepted('admin','teacher','director','attendance')
	def post(self):
		return jsonify({'status':'OK','message':'PONG'})

class AttendanceDelete(MethodView):
	def delete(self,attendance_id):
		attendance = Attendance.query.get(attendance_id)
		if attendance:
			if attendance.kid:
				last_attendance = Attendance.query.filter(Attendance.kid == attendance.kid).order_by(Attendance.time.desc()).first()
			else:
				last_attendance = Attendance.query.filter(Attendance.user == attendance.user).order_by(Attendance.time.desc()).first()
			db.session.delete(attendance)
			db.session.commit()
			if attendance == last_attendance:
				if attendance.kid:
					last_attendance = Attendance.query.filter(Attendance.kid == attendance.kid).order_by(Attendance.time.desc()).first()
				else:
					last_attendance = Attendance.query.filter(Attendance.user == attendance.user).order_by(Attendance.time.desc()).first()

				if last_attendance:
					if attendance.kid:
						attendance.kid.status = last_attendance.status
						attendance.kid.status_time = last_attendance.time
					else:
						attendance.user.status = last_attendance.status
						attendance.user.status_time = last_attendance.time
				else:
					if attendance.kid:
						attendance.kid.status = 0
						dt = datetime.datetime.utcnow()
						dt = dt.replace(tzinfo=pytz.utc)
						attendance.kid.status_time = dt
					else:
						attendance.user.status = 0
						dt = datetime.datetime.utcnow()
						dt = dt.replace(tzinfo=pytz.utc)
						attendance.user.status_time = dt
			db.session.commit()
			return jsonify({'status':'OK'})

		else:
			return jsonify({'status':'NOGOOD'})


class AttendanceEditView(MethodView):
	def get(self,attendance_id):
		attendance = Attendance.query.get(attendance_id)
		if attendance:
			return render_template('back/attendance_edit.html',attendance=attendance,attendance_edit_form=AttendanceEditForm(obj=attendance))
		else:
			flash('Attendance item not found')
			return redirect(url_for('back.attendance_log'))

	def post(self,attendance_id):
		attendance_edit_form = request.form
		attendance = Attendance.query.get(attendance_id)
		tz = pytz.timezone(current_user.timezone)
		date_time_str = str(attendance_edit_form['time_date'] + ' ' + attendance_edit_form['time_time'])
		dt = datetime.datetime.strptime(date_time_str,'%m/%d/%Y %I:%M %p')
		d_tz = tz.normalize(tz.localize(dt))
		utc = pytz.timezone('UTC')
		d_utc = d_tz.astimezone(utc)
		attendance.time = d_utc
		last_attendance = Attendance.query.filter(Attendance.kid == attendance.kid).order_by(Attendance.time.desc()).first()
		status = 0
		if 'status' in attendance_edit_form:
			attendance.status = 1
			status = 1
		else:
			attendance.status = 0

		if attendance == last_attendance:
			attendance.kid.status = status
			attendance.kid.status_time = d_utc

		db.session.commit()
		return redirect(url_for('back.attendance_log'))

class SignFormsResultsView(MethodView):
	@roles_accepted('admin','director')
	def get(self, sign_form_id):
		import json
		signatures = Signature.query.filter(
			Signature.sign_form == SignForm.query.get(sign_form_id)
		).all()
		sigs = []
		for sig in signatures:
			sigs.append(sig.serialize)

		return json.dumps(sigs)

class SignFormsEditView(MethodView):
	@roles_accepted('admin','director')
	def post(self,sign_form_id,kid_id,yesno):
		import cgi
		signature = Signature.query.filter(
			Signature.kid == Kid.query.get(kid_id),
			Signature.sign_form == SignForm.query.get(sign_form_id)
		).first()
		signature.signed = bool(int(yesno))
		if 'data' in request.form:
			signature.extra_data  = cgi.escape(request.form['data'])
		db.session.add(signature)
		db.session.commit()
		return jsonify({'result':'OK'})

class SignFormsShowView(MethodView):
	@roles_accepted('admin','director')
	def get(self,sign_form_id,kid_id):
		sign_form = SignForm.query.filter(SignForm.id == sign_form_id,SignForm.school == g.current_school).first()

		signature = Signature.query.filter(
			Signature.kid == Kid.query.get(kid_id),
			Signature.sign_form == sign_form
		).first()

		return render_template('back/show_form.html',sign_form=sign_form, signature=signature)

class AllergenForm(Form):
	name = TextField()

class SignFormView(MethodView):
	@roles_accepted('admin','director')
	def get(self,sign_form_id):
		sign_forms = SignForm.query.filter(SignForm.school==g.current_school).all()
		sign_form = SignForm.query.filter(SignForm.id == sign_form_id,SignForm.school == g.current_school).first()
		if sign_form:
			sign_forms_form=SignFormsForm(obj=sign_form)
		else:
			sign_forms_form=SignFormsForm()
		return render_template('back/sign_forms.html',sign_forms=sign_forms,sign_forms_form=sign_forms_form,sign_form=sign_form)

	@roles_accepted('admin','director')
	def post(self,sign_form_id):
		import bleach
		sign_forms_form = SignFormsForm(request.form)
		sign_form = SignForm.query.filter(SignForm.id == sign_form_id,SignForm.school == g.current_school).first()
		if sign_form and sign_forms_form.validate():
			sign_forms_form.populate_obj(sign_form)
			sign_form.content = bleach.clean(sign_forms_form.copy_html.data,tags=whitelist_html,attributes=whitelist_attr)
			sign_form.markdown = sign_forms_form.content.data
			sign_form.school = g.current_school
			db.session.commit()
			return redirect(url_for('back.sign_forms'))
		return render_template('back/sign_forms.html',sign_forms=sign_forms,sign_forms_form=sign_forms_form)

	@roles_accepted('admin','director')
	def delete(self,sign_form_id):
		sign_form = SignForm.query.filter(SignForm.id == sign_form_id,SignForm.school == g.current_school).first()
		if sign_form:
			db.session.delete(sign_form)
			db.session.commit()
			return jsonify({'result':'OK'})

class SignFormsView(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		sign_forms = SignForm.query.filter(SignForm.school==g.current_school).all()

		return render_template('back/sign_forms.html',sign_forms=sign_forms,sign_forms_form=SignFormsForm())

	@roles_accepted('admin','director')
	def post(self):
		import bleach
		sign_forms_form = SignFormsForm(request.form)
		if sign_forms_form.validate():
			sign_form = SignForm()
			sign_form.title = sign_forms_form.title.data
			sign_form.content = bleach.clean(sign_forms_form.copy_html.data,tags=whitelist_html,attributes=whitelist_attr)
			sign_form.markdown = sign_forms_form.content.data
			for kid in sign_forms_form.kids.data:
				sign_form.signatures.append(Signature(kid))
			sign_form.active = sign_forms_form.active.data
			sign_form.requires_signature = sign_forms_form.requires_signature.data
			sign_form.school = g.current_school
			db.session.add(sign_form)
			db.session.commit()
			return redirect(url_for('back.sign_forms'))
		return render_template('back/sign_forms.html',sign_forms=sign_forms,sign_forms_form=sign_forms_form)

class SettingsImportStatus(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		import json
		return jsonify({'kids':g.current_school.kids.count()})

class TestingView(MethodView):
	@roles_accepted('admin','director')
	def post(self,testing_mode):
		if testing_mode == '0':
			g.current_school.testing = False
		else:
			g.current_school.testing = True

		db.session.commit()

		return jsonify({'result' : 'OK'})

class AttendanceDownloadForm(Form):
	from datetime import timedelta
	import time
	dt = datetime.datetime.now()
	start = dt - timedelta(days = dt.weekday())
	end = start + timedelta(days = 6)
	from_attendance = TextField('from',default=start.strftime('%m/%d/%Y'))
	to_attendance = TextField('to',default=end.strftime('%m/%d/%Y'))

	def validate(self):
		import time
		rv = Form.validate(self)
		if not rv:
			return False
		#validate the uname for uniqueness
		try:
			from_date = time.strptime(self.from_attendance.data,'%m/%d/%Y')
		except ValueError:
			self.from_attendance.errors.append('Please format date as mm/dd/yyyy')
			return False

		try:
			to_date = time.strptime(self.to_attendance.data,'%m/%d/%Y')
		except ValueError:
			self.to_attendance.errors.append('Please format date as mm/dd/yyyy')
			return False

		if from_date > to_date:
			self.to_attendance.errors.append('From-date should be before to-date')
			self.from_attendance.errors.append('From-date should be before to-date')
			return False
		return True

class AttendanceDownload(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		from flask import Response
		import xlwt
		import StringIO
		import mimetypes
		import time
		import random
		import string

		form = AttendanceDownloadForm(request.args)

		if 'from_attendance' in request.args and 'to_attendance' in request.args:
			if not form.validate():
				return render_template('back/attendance_report.html',attendance_download_form=form);			
			from_date = datetime.datetime.strptime(request.args['from_attendance'],'%m/%d/%Y')
			to_date = datetime.datetime.strptime(request.args['to_attendance'],'%m/%d/%Y')
		else:
			return redirect(url_for('back.reports'));

		response = Response()
		response.status_code = 200
		workbook = xlwt.Workbook()
		kids = g.current_school.kids.all()

		groups = g.current_school.groups.all()

		def daterange(start_date, end_date):
			temp_list = []
			for n in range(int ((end_date - start_date).days)):
				temp_list.append(start_date + datetime.timedelta(n))

			return temp_list

		date_list = daterange(from_date,to_date)

		for i,group in enumerate(groups):
			try:
				sheet = workbook.add_sheet(group.name)
				sheet2 = workbook.add_sheet('%s-count' % group.name)
			except:
				random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
				sheet = workbook.add_sheet('%s-%s' % (group.name, random_string))
				sheet2 = workbook.add_sheet('%s-%s-count' % (group.name, random_string))
			for k,day in enumerate(date_list):
				sheet.write(0,1+k,day.strftime('%a %m/%d/%y'))

			sheet2.write(0,1,'IN COUNT')

			for j,kid in enumerate(group.kids.all()):
				sheet.write(j+1,0,kid.full_name)
				sheet2.write(j+1,0,kid.full_name)

				has_between = False
				has_one_for_all = False
				has_calendar = False
				if kid.between_schedule:
					has_between = True
				elif kid.one_for_all_schedule:
					has_one_for_all = True
				elif kid.calendar_schedule:
					has_calendar = True
				
				kid_in_count = 0
				for m,day in enumerate(date_list):
					next_day = day + datetime.timedelta(days=1)
					in_time = kid.attendances.filter(Attendance.time >= day,Attendance.time < next_day,Attendance.status == 1).first()
					out_time = kid.attendances.filter(Attendance.time >= day,Attendance.time < next_day,Attendance.status == 0).first()

					in_status = 'IN:NO SCHEDULE'
					out_status = 'OUT:NO SCHEDULE'
					if in_time and out_time:
						kid_in_count += 1
						time_format = '%I:%M %p'
						from_string = in_time.time.strftime(time_format)
						to_string = out_time.time.strftime(time_format)

						if has_between:
							this_day_start_time = getattr(kid.between_schedule,'%s_start' % weekday_list[day.weekday()])
							if in_time.time.time() < this_day_start_time:
								in_status = 'IN:EARLY (%s)' % this_day_start_time.strftime(time_format)
							else:
								in_status = 'IN:OK'


							this_day_end_time = getattr(kid.between_schedule,'%s_end' % weekday_list[day.weekday()])
							if out_time.time.time() > this_day_end_time:
								out_status = 'OUT:LATE (%s)' % this_day_end_time.strftime('%I:%M %p')
							else:
								out_status = 'OUT:OK'

						elif has_one_for_all:
							in_out_status = 'OK'
						elif has_calendar:
							in_out_status = 'OK'

						sheet.write(j+1,m+1,'%s-%s\n%s-%s' % (from_string,to_string,in_status,out_status) )

				sheet2.write(j+1,1,str(kid_in_count))


		output = StringIO.StringIO()
		workbook.save(output)
		response.data = output.getvalue()

		filename = 'export.xls'
		mimetype_tuple = mimetypes.guess_type(filename)

		#HTTP headers for forcing file download
		response.headers.add('Pragma','public')
		response.headers.add('Content-Type',mimetype_tuple[0])
		response.headers.add('Content-Disposition','attachment; filename=\"%s\";' % filename)
		response.headers.add('Cache-Control','private')
		response.headers.add('Content-Transfer-Encoding','binary')
		response.headers.add('Content-Length',len(response.data))

		if not mimetype_tuple[1] is None:
			response.update({
				'Content-Encoding': mimetype_tuple[1]
			})

		#as per jquery.fileDownload.js requirements
		response.set_cookie('fileDownload', 'true', path='/')

		return response

class ScholarshipDownload(MethodView):
	@roles_accepted('admin','director')
	def get(self):
		from flask import Response
		import xlwt
		import StringIO
		import mimetypes

		response = Response()
		response.status_code = 200
		workbook = xlwt.Workbook()
		sheet = workbook.add_sheet("Sheet Name") 
		sheet.write(0, 0, 'foobar') # row, column, value

		output = StringIO.StringIO()
		workbook.save(output)
		response.data = output.getvalue()

		filename = 'export.xls'
		mimetype_tuple = mimetypes.guess_type(filename)

		#HTTP headers for forcing file download
		response.headers.add('Pragma','public')
		response.headers.add('Content-Type',mimetype_tuple[0])
		response.headers.add('Content-Disposition','attachment; filename=\"%s\";' % filename)
		response.headers.add('Cache-Control','private')
		response.headers.add('Content-Transfer-Encoding','binary')
		response.headers.add('Content-Length',len(response.data))

		if not mimetype_tuple[1] is None:
			response.update({
				'Content-Encoding': mimetype_tuple[1]
			})

		#as per jquery.fileDownload.js requirements
		response.set_cookie('fileDownload', 'true', path='/')

		return response

class WeeklyHoursView(MethodView):
	def post(self,kid_id):
		data = request.form
		start_time = datetime.datetime.strptime(data['start_time'],'%Y/%m/%d %I:%M %p')
		end_time = datetime.datetime.strptime(data['end_time'],'%Y/%m/%d %I:%M %p')
		if 'start_time' in data and 'end_time' in data and 'kid' in data:
			weekly_hour = WeeklyHour(start_time=start_time,
				end_time=end_time,
				kid=Kid.query.get(data['kid']),
				school=g.current_school)
			db.session.add(weekly_hour)
			db.session.commit()
			display_time = '%s-%s' % (start_time.strftime('%I:%M %p'),end_time.strftime('%I:%M %p'))
		return jsonify({'status':'OK','display':display_time})

class WeeklyHoursByDateView(MethodView):
	def post(self,kid_id):
		import json

		week_date = request.form['week_date']

		kid = Kid.query.get(kid_id)
		week_dt = datetime.datetime.strptime(week_date,'%Y-%m-%d')

		if(request.endpoint == 'back.weekly_hours_by_date_next_view'):
			# NEXT WEEK
			week_dt = week_dt + datetime.timedelta(days=7)
		elif(request.endpoint == 'back.weekly_hours_by_date_prev_view'):
			# PREV WEEK
			week_dt = week_dt - datetime.timedelta(days=7)

		week_dt = week_dt - datetime.timedelta(hours=week_dt.hour,minutes=week_dt.minute,seconds=week_dt.second,microseconds=week_dt.microsecond)
		week_dates = [week_dt + datetime.timedelta(days=i) for i in xrange(0 - week_dt.weekday(), 7 - week_dt.weekday())]
		weekly_schedule = WeeklyHour.query.filter(
			or_(
				and_(
					WeeklyHour.start_time >= week_dates[0],
					WeeklyHour.end_time >= week_dates[0],
					WeeklyHour.kid == kid
					),
				and_(
					WeeklyHour.start_time <= week_dates[-1],
					WeeklyHour.end_time <= week_dates[-1],
					WeeklyHour.kid == kid
					)
				)
			).all()

		weekly_schedule_with_dates = []
		for day in week_dates:
			if weekly_schedule:
				matched = False
				for schedule_day in weekly_schedule:
					if schedule_day.start_time.date() == day.date():
						weekly_schedule_with_dates.append({
							'date':day.date().strftime('%Y/%m/%d'),
							'date_dash':day.date().strftime('%Y-%m-%d'),
							'schedule_day':{
							'start_time':schedule_day.start_time.strftime('%I:%M %p'),
							'end_time':schedule_day.end_time.strftime('%I:%M %p'),
							},
							'day_of_week':day.date().strftime('%a'),
							'day_with_date':day.date().strftime('%a %m/%d/%y')
						})
						matched = True
						break;
				
				if not matched:
					weekly_schedule_with_dates.append({
						'date':day.date().strftime('%Y/%m/%d'),
						'date_dash':day.date().strftime('%Y-%m-%d'),
						'schedule_day':None,
						'day_of_week':day.date().strftime('%a'),
						'day_with_date':day.date().strftime('%a %m/%d/%y')
					});

			else:
				weekly_schedule_with_dates.append({
					'date':day.date().strftime('%Y/%m/%d'),
					'date_dash':day.date().strftime('%Y-%m-%d'),
					'schedule_day':None,
					'day_of_week':day.date().strftime('%a'),
					'day_with_date':day.date().strftime('%a %m/%d/%y')
				})


		return json.dumps(weekly_schedule_with_dates)


class TokenRequestView(MethodView):
	def get(self):
		return jsonify({'result':'token','token':current_user.get_mobile_access_token()})

class AttendanceReport(MethodView):
	@roles_accepted('admin','director')
	def get(self,attendance_date=None):
		# 915am monday april 14 2014
		dt_s = '9:15am 4/14/2014'
		dt = datetime.datetime.strptime(dt_s,'%I:%M%p %m/%d/%Y')
		WeeklyHour.query.filter(
			and_(
					WeeklyHour.start_time >= dt,
					WeeklyHour.end_time <= dt,
				),
		).count()
		return render_template('back/attendance_graph.html')	

class DailySheetMailView(MethodView):
	@roles_accepted('admin','director')
	def post(self,kid_id):
		kid = Kid.query.get(kid_id)
		date = datetime.datetime.now().strftime('%m/%d/%Y')
		attendance = Attendance.query.filter(Attendance.kid == kid).order_by(Attendance.time.desc()).limit(10)
		activities = kid.activities.order_by(Activity.id.desc()).limit(10)

		m = mandrill.Mandrill(faycare.apikeys.mandrill_api,True)

		daily_sheet = render_template('back/email/daily_report.html',
			date=date,
			attendance=attendance,
			activities=activities)

		full_name = "Parent"
		subject = '[%s] Your daily sheet is available!' % g.current_school.name
		tags = ['daily report']

		message = get_message_obj_multi(
			daily_sheet,
			subject,
			kid.parents,
			tags
			)

		if not g.testing:
			m.messages.send(message)

		return jsonify({'status':'OK'})

class SettingsImportView(MethodView):
	@roles_accepted('admin','director')
	def post(self):
		# Save the file
		from werkzeug import secure_filename
		from xlrd import open_workbook
		from xlrd import XLRDError
		import os
		import time
		import base64
		import uuid
		import string
		import random

		messages = []
		self.test_count = 0
		self.insert_count = 0
		temp_file = ''
		# OPEN FILE
		file = request.files['import_file']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			temp_file = os.path.join(config['UPLOAD_FOLDER'], filename)
			file.save(temp_file)
			funky_name = slugify(unicode('_'.join([g.current_school.name,current_user.person.first_name,str(time.time()),base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')])))
			new_funky_path = os.path.join(config['UPLOAD_FOLDER'], funky_name)
			os.rename(temp_file, new_funky_path)
			temp_record = TempFile(time=datetime.datetime.utcnow(),path=new_funky_path)
			db.session.add(temp_record)
			db.session.commit()

			try:
				wb = open_workbook(new_funky_path)
				self.sheet = wb.sheets()[0]
				ok = True
				num_kids = g.current_school.kids.count() + self.sheet.nrows
				if num_kids > g.current_school.company.plan.limit and g.current_school.company.plan.name == 'free':
					messages.append('The free plan allows %s tots max. Paid plans do not have a hard limit. You are trying to import %s tots. You can upgrade to a paid plan under Settings->Account.' % (g.current_school.company.plan.limit,self.sheet.nrows))
					ok = False
					return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})
			except XLRDError, e:
				messages.append('There was something not quite right about the xls/xlsx file you gave us. %s' % e)
				ok = False
				return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})
		else:
			messages.append('We need the right file type or we did not get a file.')
			ok = False
			return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})

		def run_import(self,test):
			messages = []
			headers = list(config['XLS_HEADERS'])
			ok = True
			ok_count = 0
			new_headers = []
			headers = [x.upper() for x in headers]

			self.between_schedules_all = BetweenSchedule.query.all()
			self.calendar_schedules_all = CalendarSchedule.query.all()
			self.one_for_all = OneForAllSchedule.query.all()

			for col in range(self.sheet.ncols):
				if self.sheet.cell(0,col).value.upper() in (name for name in headers):
					ok_count += 1
					new_headers.append(self.sheet.cell(0,col).value.lower())
				else:
					messages.append('Column header %s is not one of our accepted headers. We just ignored it.' % self.sheet.cell(0,col).value)

			if ok_count != len(headers):
				messages.append('Looks like you are missing one or more header columns. We could not import because of missing data. Fix your data and try uploading again. No records were created or updated.')
				ok = False
			else:
				ok = True

			if not ok:
				return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})

			# 'group']
			# We have checkted the data for legitness. Now we'll create the records if the data is ok.
			if ok:
				ok = True
				for row in range(self.sheet.nrows):
					user_exists = False
					kid = Kid()
					person = Person()
					if row == 0:
						continue
					for col in range(len(new_headers)):
						header = new_headers[col]
						cell = self.sheet.cell(row,col).value
						if header == 'student first name':
							kid.first_name = cell.title()
						elif header == 'student last name':
							kid.last_name = cell.title()
						elif header == 'student birthdate':
							if isinstance( cell, ( int, long, float ) ):
								ok = False
								messages.append('Birthdays must be written as mm/dd/yyyy and formatted as plain text. You gave us a number: %f' % cell)
							else:
								try:
									date_object = datetime.datetime.strptime(cell, '%m/%d/%Y')
									kid.dob = date_object
								except ValueError:
									try:
										date_object = datetime.datetime.strptime(cell, '%m/%d/%y')
										kid.dob = date_object
									except ValueError:
										messages.append('Dates must be formatted as mm/dd/yyyy')
										ok = False

						elif header == 'student gender':
							if cell.lower() == 'male':
								kid.gender = 'm'
							elif cell.lower() == 'female':
								kid.gender = 'f'
							elif cell.lower() == 'f':
								kid.gender = 'f'
							elif cell.lower() == 'm':
								kid.gender = 'm'
							elif cell.lower() == 'girl':
								kid.gender = 'f'
							elif cell.lower() == 'boy':
								kid.gender = 'f'
							else:
								messages.append('Gender must be written as male,female or m,f or girl,boy')
						elif header == 'guardian first name':
							person.first_name = cell.title()
						elif header == 'guardian last name':
							person.last_name = cell.title()
						elif header == 'guardian cell phone':
							if len(cell) > 20:
								ok = False
								messages.append('20 characters is the max for phone numbers. You have exceeded that with %s' % cell)
							person.cell_phone = cell
						elif header == 'guardian home phone':
							if len(cell) > 20:
								ok = False
								messages.append('20 characters is the max for phone numbers. You have exceeded that with %s' % cell)
							person.home_phone = cell
						elif header == 'guardian email':
							if not test:
								if not cell:
									randomstring = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
									cell = 'unknown_user%s@gototgo.com' % randomstring
									messages.append('A user did not have an email listed. Users need an email to sign in. We made a temporary one: %s. Please change this email.' % cell)
								user = user_datastore.get_user(cell)
								if user:
									user_exists = True
								else:
									temp_password = sample_string()
									user = user_datastore.create_user(
										email=cell.lower(),
										password=encrypt_password(temp_password)
									)
									user_datastore.add_role_to_user(user,'parent')
						elif header == 'address':
							person.street = cell
						elif header == 'city':
							person.city = cell
						elif header == 'state':
							person.state = cell
						elif header == 'zip':
							person.zip_code = cell
						elif header == 'group':
							if not test:
								already_exists_group = Group.query.filter(func.lower(Group.name) == func.lower(cell)).first()
								if(already_exists_group):
									group = already_exists_group
								else:
									group = Group()
									if not cell:
										group.name = 'Unknown Group'
									else:
										group.name = cell.title()
									for between_hours in self.between_schedules_all:
										p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
										between_hours.prices.append(p)

									for calendar_hours in self.calendar_schedules_all:
										p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
										calendar_hours.prices.append(p)

									for one_for_all in self.one_for_all:
										p = Price(billing_duration=30,price=5.00,group=group,billing_unit=1)
										one_for_all.prices.append(p)

									group.school = g.current_school
						if not ok:
							messages.append('We created %i records' % self.insert_count)
							messages.append('We tested %i records' % self.test_count)
							return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})

					if ok:
						#Set all the relationships
						if not test:
							if not user_exists:
								user.person = person
							user.kids.append(kid)
							kid.school = g.current_school
							kid.user = user
							kid.status = 0
							dt = datetime.datetime.utcnow()
							dt = dt.replace(tzinfo=pytz.utc)
							kid.status_time = dt
							user.schools.append(g.current_school)
							user.pending = False
							kid.groups.append(group)
							kid.current_group = group.name

							try:
								self.insert_count += 1
								user_status = UserImportStatus.query.filter(UserImportStatus.user == current_user).first()
								if not user_status:
									user_status = UserImportStatus(user=current_user)
								user_status.status = self.insert_count
								db.session.add(user_status)
								db.session.commit()
							except Exception,e:
								ok = False
								db.session.rollback()
								messages.append('Your data is not quite right. We could not save your data. Error: %s' % e)
						else:
							self.test_count += 1
					else:
						messages.append('Your data is not quite right. Make sure each cell is formatted to plain text. We could not save your data.')
						messages.append('We created %i records' % self.insert_count)
						messages.append('We tested %i records' % self.test_count)
						return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})

					if not ok:
						messages.append('We created %i records' % self.insert_count)
						messages.append('We tested %i records' % self.test_count)
						return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})
			if not test:
				print 'committing'
				messages.append('We created %i records' % self.insert_count)
				messages.append('We tested %i records' % self.test_count)
				return jsonify({'message':'OK','messages':','.join(messages),'ok':ok})
			else:
				return run_import(self,False)

		return run_import(self,True)

class NewSchoolForm(Form):
	name = TextField('Name of new center',validators=[validators.Required()])
	city = TextField(validators=[validators.Required()])
	state = TextField('State abbv',validators=[validators.Required()])

class BillEditForm(Form):
	active = BooleanField('Approved')
	amount = TextField()
	per_day = TextField()
	notified = BooleanField()
	description = TextAreaField()
	status = TextField()

class CreditForm(Form):
	amount = TextField(validators=[validators.Required()])
	description = TextAreaField(validators=[validators.Required()])

class InvoiceEditForm(Form):
	paid = TextField('Amount paid')
	paid_off = BooleanField()

class PersonalInfoForm(Form):
	first_name = TextField(validators=[validators.Required()])
	middle_name = TextField()
	last_name = TextField(validators=[validators.Required()])
	cell_phone = TextField()
	home_phone = db.Column(db.String(20))
	work_phone = db.Column(db.String(20))
	street = db.Column(db.String())
	city = db.Column(db.String())
	state = db.Column(db.String())
	zip_code = db.Column(db.String())

class StaffForm(PersonalInfoForm,forms.UniqueEmailFormMixin):
	groups = QuerySelectMultipleField('groups',query_factory=get_group_query_factory,get_label='name')
	is_director = BooleanField('Staff is the director?')
	code = TextField('Code',validators=[validators.Required(),code_check])

class StaffEditForm(PersonalInfoForm):
	groups = QuerySelectMultipleField('groups',query_factory=get_group_query_factory,get_label='name')
	pending = BooleanField('Pending')
	code = TextField('Code',validators=[validators.Required(),code_check])

class BirthdateForm(Form):
	month = SelectField(choices=months_list)
	day = SelectField(choices=day_list)
	year = SelectField(choices=year_list)

class ParentForm(StaffForm):
	kids = QuerySelectMultipleField('Select Kids',query_factory=get_kids_query_factory,get_label='full_name')

class GroupsForm(Form):
	name = TextField(validators=[validators.Required()])
	kids = QuerySelectMultipleField('Select Kids',query_factory=get_kids_query_factory,get_label='full_name')
	staff = QuerySelectMultipleField('Select Staff',query_factory=get_staff_query_factory,get_label='person.full_name')

class ExtraCreditForm(Form):
	scholarship = DecimalField('Scholarship per month',validators=[validators.NumberRange(min=0)])
	govt = DecimalField('Government assistance per month',validators=[validators.NumberRange(min=0)])
	extra_credit = DecimalField('Other money credited per month',validators=[validators.NumberRange(min=0)])

class AllergenSelectForm(Form):
	allergens = QuerySelectMultipleField('allergens',query_factory=get_allergens_query_factory,get_label='name')

class KidsForm(PersonalInfoForm,ExtraCreditForm,AllergenSelectForm):
	parents = QuerySelectMultipleField('parents',query_factory=get_parents_query_factory,get_label='person.first_name')
	groups = QuerySelectMultipleField('groups',query_factory=get_group_query_factory,get_label='name')
	hold = BooleanField('On Hold?')

class ParentsKidsForm(PersonalInfoForm):
	pass

class ActivityForm(Form):
	activity_type = SelectField(coerce=int, choices=[(a.id,a.name) for a in ActivityType.query.all()])
	description = TextAreaField()

class AttendanceForm(Form):
	code = TextField('enter your code')

class MessageForm(Form):
	message_kid = HiddenField()
	message_parent = HiddenField()
	message_group = HiddenField()
	message_staff = HiddenField()
	message_message = TextAreaField('Send a message...')

class BetweenHoursForm(Form):
	__visual_hours = ['%s:%s%s' % (h, m, ap) for ap in ('am', 'pm') for h in ([12] + range(1,12)) for m in ('00', '15','30','45')]
	__computed_hours = ['%02d:%s:00' % (h, m) for h in (range(0,24)) for m in ('00', '15','30','45')]
	__hours = [(__computed_hours[r],__visual_hours[r]) for r in range(0,len(__visual_hours))]
	name = TextField()
	mon_start = SelectField('Start Monday',choices = __hours)
	mon_end = SelectField('End Monday',choices = __hours)
	tues_start = SelectField('Start Tuesday',choices = __hours)
	tues_end = SelectField('End Tuesday',choices = __hours)
	weds_start = SelectField('Start Wednesday',choices = __hours)
	weds_end = SelectField('End Wednesday',choices = __hours)
	thurs_start = SelectField('Start Thursday',choices = __hours)
	thurs_end = SelectField('End Thursday',choices = __hours)
	fri_start = SelectField('Start Friday',choices = __hours)
	fri_end = SelectField('End Friday',choices = __hours)
	sat_start = SelectField('Start Saturday',choices = __hours)
	sat_end = SelectField('End Saturday',choices = __hours)
	sun_start = SelectField('Start Sunday',choices = __hours)
	sun_end = SelectField('End Sunday',choices = __hours)
	closed_mon = BooleanField('No hours mon')
	closed_tues = BooleanField('No hours tues')
	closed_weds = BooleanField('No hours weds')
	closed_thurs = BooleanField('No hours thurs')
	closed_fri = BooleanField('No hours fri')
	closed_sat = BooleanField('No hours sat')
	closed_sun = BooleanField('No hours sun')

class OneForAllForm(Form):
	name = TextField()

class SchoolHoursForm(BetweenHoursForm):
	closed_mon = BooleanField()
	closed_tues = BooleanField()
	closed_weds = BooleanField()
	closed_thurs = BooleanField()
	closed_fri = BooleanField()
	closed_sat = BooleanField()
	closed_sun = BooleanField()

class HourlyForm(OneForAllForm):
	pass

class CalendarForm(OneForAllForm):
	pass

class SchedulePriceForm(Form):
	price = TextField()
	billing_duration = TextField('Billing cycle (in days)')
	billing_unit = SelectField(choices=billing_units)
	late_price = TextField('Late price per hour')

class ScheduleEqualPriceForm(Form):
	price = TextField()
	late_price = TextField('Late price per hour')

class RejectionForm(Form):
	reason = TextAreaField()

class CustomerNewForm(Form):
	plan = HiddenField()

class CustomerNewParentForm(Form):
	amount = TextField()

class ChangePlanForm(CustomerNewForm):
	pass

class VaccineForm(Form):
	vaccine = TextField()
	due_dates = FieldList(TextField('Age due (in months)', [validators.required()]),min_entries=1)

class ScheduleKidsForm(Form):
	kids = QuerySelectMultipleField('Pick your tots',query_factory=get_kids_with_groups_factory,get_label='full_name')

class SignFormsForm(Form):
	title = TextField()
	content = TextAreaField()
	kids = QuerySelectMultipleField('Select Kids',query_factory=get_kids_query_factory,get_label='full_name')
	requires_signature = BooleanField('Requires signature?')
	copy_html = TextAreaField()
	active = BooleanField('Active?')

class AttendanceEditForm(Form):
	time = DateTimeField()
	status = BooleanField('IN?')

class OrderForm(Form):
	title = TextField(validators=[validators.Required()])
	content = TextAreaField()
	order_type = QuerySelectField(query_factory=get_order_type_query_factory,get_label='name')
	due_date = TextField('Due (mm/dd/yyyy)')
	assignee = QuerySelectField('Select assignee',query_factory=get_staff_query_factory,get_label='person.full_name')
	tags = TextField()
	priority = QuerySelectField(query_factory=get_order_priority_query_factory,get_label='name')

	def validate(self):
		import time
		rv = Form.validate(self)
		if not rv:
			return False
		try:
			due_date = time.strptime(self.due_date.data,'%m/%d/%Y')
			return True
		except ValueError:
			self.due_date.errors.append('Please format date as mm/dd/yyyy')
			return False

class CommentForm(Form):
	content = TextAreaField(validators=[validators.Required()])

class AgeRangeForm(Form):
	start_age = IntegerField('From age (in months)',validators=[validators.Required(),validators.NumberRange(min=0)])
	end_age = IntegerField('From to (in months)',validators=[validators.Required(),validators.NumberRange(min=0)])
	needs_potty_trained = BooleanField('Only potty trained age range')



class CustomBillForm(Form):
	description = TextAreaField(validators=[validators.Required()])
	amount = TextField(validators=[validators.Required()])
	per_day = TextField(validators=[validators.Required()])
	custom_bill_kid = QuerySelectField('Choose a tot',query_factory=get_kids_query_factory,get_label='full_name')

class BillByKidForm(Form):
	bill_kid = QuerySelectField('Choose a tot',query_factory=get_kids_query_factory,get_label='full_name')


back.add_url_rule('/',view_func=HomeView.as_view('home'))
back.add_url_rule('/in/',view_func=InView.as_view('kids_in'))
back.add_url_rule('/out/',view_func=OutView.as_view('kids_out'))
back.add_url_rule('/import/',view_func=ImportView.as_view('import'))
back.add_url_rule('/import/file/',view_func=SettingsImportView.as_view('settings_import'))
back.add_url_rule('/import/status/',view_func=SettingsImportStatus.as_view('settings_import_status'))
back.add_url_rule('/messages/',view_func=MessagesView.as_view('messages'))
back.add_url_rule('/kids/',view_func=KidsView.as_view('kids'))
back.add_url_rule('/kids/new/',view_func=KidsNewView.as_view('kids_new'))
back.add_url_rule('/kids/<kid_id>/',view_func=KidsShowView.as_view('kids_show'))
back.add_url_rule('/kids/more/<page>/',view_func=KidsMoreView.as_view('kids_more'))
back.add_url_rule('/kids/edit/<kid_id>/',view_func=KidsEditView.as_view('kids_edit'))
back.add_url_rule('/kids/<kid_id>/activities/new/',view_func=KidsActivitiesNew.as_view('kids_activities_new'))
back.add_url_rule('/kids/<kid_id>/upload/file/',view_func=KidsUploadFile.as_view('kids_upload_file'))
back.add_url_rule('/kids/<kid_id>/upload/activity/file/',view_func=KidsUploadFile.as_view('kids_upload_activity_file'))
back.add_url_rule('/kids/<kid_id>/photos/<photo_id>/<size_id>/',view_func=KidsPhotos.as_view('kids_photos'))
back.add_url_rule('/kids/<kid_id>/remove/file/',view_func=KidsRemoveFile.as_view('kids_remove_file'))
back.add_url_rule('/kids/search/',view_func=KidsSearchView.as_view('kids_search'))
back.add_url_rule('/groups/',view_func=GroupsView.as_view('groups'))
back.add_url_rule('/groups/edit/<group_id>/',view_func=GroupsEditView.as_view('groups_edit'))
back.add_url_rule('/groups/<group_id>/',view_func=GroupsShowView.as_view('groups_show'))
back.add_url_rule('/groups/<group_id>/activities/new/',view_func=GroupsActivitiesNew.as_view('groups_activities_new'))
back.add_url_rule('/groups/<group_id>/kids/',view_func=GroupsKidsView.as_view('groups_kids'))
back.add_url_rule('/age_ranges/',view_func=AgeRangeView.as_view('age_ranges'))
back.add_url_rule('/customer/new/',view_func=CustomerNewView.as_view('customer_new'))
back.add_url_rule('/customer/receipts/',view_func=CustomerReceiptsView.as_view('customer_receipts'))
back.add_url_rule('/customer/update/',view_func=CustomerUpdateView.as_view('customer_update'))
back.add_url_rule('/bills/',view_func=BillsView.as_view('bills'))
back.add_url_rule('/bills/edit/<bill_id>/',view_func=BillsEditView.as_view('bills_edit'))
back.add_url_rule('/bills/<kid_id>/',view_func=BillsView.as_view('bills_kids_single'))
back.add_url_rule('/testing/<testing_mode>/',view_func=TestingView.as_view('testing'))
back.add_url_rule('/bills/kids/',view_func=BillsView.as_view('bills_kid'))
back.add_url_rule('/bills/groups/',view_func=BillsView.as_view('bills_group'))
back.add_url_rule('/bills/activate/<bill_id>/',view_func=BillsActivateView.as_view('bills_activate'))
back.add_url_rule('/bills/to/<kid_id>/from/<month>/<year>/',view_func=BillsSingleView.as_view('bill_single'))
back.add_url_rule('/bills/charge/<kid_id>/',view_func=BillsChargeCard.as_view('bills_charge'))
back.add_url_rule('/credits/<invoice_id>/<bill_id>/',view_func=CreditsView.as_view('credits_new'))
back.add_url_rule('/staff/',view_func=StaffView.as_view('staff'))
back.add_url_rule('/staff/edit/<staff_id>/',view_func=StaffEditView.as_view('staff_edit'))
back.add_url_rule('/staff/<staff_id>/',view_func=StaffShowView.as_view('staff_show'))
back.add_url_rule('/parents/',view_func=ParentsView.as_view('parents'))
back.add_url_rule('/parents/<parent_id>/',view_func=ParentsView.as_view('parents_show'))
back.add_url_rule('/parents/edit/<parent_id>/',view_func=ParentsEditView.as_view('parents_edit'))
back.add_url_rule('/schedules/',view_func=SchedulesView.as_view('schedules'))
back.add_url_rule('/schedules/new/',view_func=SchedulesNewView.as_view('schedules_new'))
back.add_url_rule('/schedules/school_hours/reset/',view_func=SchoolHoursReset.as_view('school_hours_reset'))
back.add_url_rule('/schedules/<schedule_id>/',view_func=SchedulesDeleteView.as_view('schedules_delete'))
back.add_url_rule('/schedules/<schedule_type>/<schedule_id>/kids/',view_func=SchedulesNewKidsView.as_view('schedules_new_kids'))
back.add_url_rule('/schedules/<schedule_type>/<schedule_id>/prices/',view_func=SchedulesPricesView.as_view('schedules_edit_prices'))
back.add_url_rule('/schedules/<schedule_type>/<schedule_id>/days/',view_func=SchedulesDaysView.as_view('schedules_edit_days'))
back.add_url_rule('/schedules/<schedule_type>/<schedule_id>/activate/',view_func=SchedulesActivateView.as_view('schedules_activate'))
back.add_url_rule('/schedules/between/',view_func=SchedulesBetweenView.as_view('schedules_between'))
back.add_url_rule('/schedules/school_hours/',view_func=SchedulesSchoolHoursView.as_view('schedules_school_hours'))
back.add_url_rule('/schedules/one_for_all/',view_func=SchedulesForAllView.as_view('schedules_for_all'))
back.add_url_rule('/schedules/hourly/',view_func=SchedulesHourlyView.as_view('schedules_hourly'))
back.add_url_rule('/schedules/calendar/',view_func=SchedulesCalendarView.as_view('schedules_calendar'))
back.add_url_rule('/schedules/calendar/set/',view_func=SchedulesSetCalendarView.as_view('schedules_calendar_set'))
back.add_url_rule('/schedules/calendar/get/<kid_id>/',view_func=SchedulesGetCalendarView.as_view('schedules_calendar_get'))
back.add_url_rule('/schedules/calendar/approve/<kid_id>/',view_func=SchedulesCalendarApproveView.as_view('schedules_calendar_approve'))
back.add_url_rule('/prices/edit/<price_id>/',view_func=PricesEditView.as_view('prices_edit'))
back.add_url_rule('/schools/new/',view_func=SchoolsNewView.as_view('schools_new'))
back.add_url_rule('/forms/',view_func=SignFormsView.as_view('sign_forms'))
back.add_url_rule('/forms/<sign_form_id>/',view_func=SignFormView.as_view('sign_forms_singular'))
back.add_url_rule('/forms/<sign_form_id>/show/<kid_id>/',view_func=SignFormsShowView.as_view('sign_forms_show'))
back.add_url_rule('/forms/<sign_form_id>/edit/<kid_id>/<yesno>/',view_func=SignFormsEditView.as_view('sign_forms_edit'))
back.add_url_rule('/forms/<sign_form_id>/results/',view_func=SignFormsResultsView.as_view('sign_forms_results'))
back.add_url_rule('/settings/',view_func=SettingsView.as_view('settings'))
back.add_url_rule('/reports/',view_func=ReportsView.as_view('reports'))
back.add_url_rule('/reports/attendance/',view_func=AttendanceReport.as_view('attendance_reports'))
back.add_url_rule('/reports/attendance/<attendance_date>/',view_func=AttendanceReport.as_view('attendance_reports_by_day'))
back.add_url_rule('/attendance/',view_func=AttendanceView.as_view('attendance'))
back.add_url_rule('/attendance/log/',view_func=AttendanceLogView.as_view('attendance_log'))
back.add_url_rule('/attendance/log/<log_date>/',view_func=AttendanceLogView.as_view('attendance_log_with_date'))
back.add_url_rule('/attendance/staff/<staff_id>/',view_func=AttendanceLogView.as_view('attendance_staff_log'))
back.add_url_rule('/attendance/staff/<staff_id>/<log_date>/',view_func=AttendanceLogView.as_view('attendance_staff_log_with_date'))
back.add_url_rule('/attendance/login/',view_func=AttendanceLoginView.as_view('attendance_login'))
back.add_url_rule('/attendance/status/',view_func=AttendanceStatusView.as_view('attendance_status'))
back.add_url_rule('/attendance/ping/',view_func=AttendancePing.as_view('attendance_ping'))
back.add_url_rule('/attendance/edit/<attendance_id>/',view_func=AttendanceEditView.as_view('attendance_edit'))
back.add_url_rule('/attendance/delete/<attendance_id>/',view_func=AttendanceDelete.as_view('attendance_delete'))
back.add_url_rule('/activities/<activity_id>/',view_func=ActivitiesView.as_view('activities_general'))
back.add_url_rule('/rejection/',view_func=RejectionView.as_view('rejection'))
back.add_url_rule('/medical/',view_func=MedicalView.as_view('medical'))
back.add_url_rule('/allergens/',view_func=AllergensView.as_view('allergens'))
back.add_url_rule('/allergens/<allergen_id>/',view_func=AllergensView.as_view('allergens_single'))
back.add_url_rule('/acceptance/',view_func=AcceptanceView.as_view('acceptance'))
back.add_url_rule('/vaccinations/',view_func=VaccinationView.as_view('vaccination'))
back.add_url_rule('/vaccinations/<vaccination_id>/',view_func=VaccinationView.as_view('vaccination_single'))
back.add_url_rule('/orders/',view_func=OrderView.as_view('orders'))
back.add_url_rule('/orders/<order_id>/',view_func=OrderView.as_view('orders_single'))
back.add_url_rule('/orders/edit/<order_id>/',view_func=OrderView.as_view('orders_single_edit'))
back.add_url_rule('/orders/edit/<order_id>/comment/',view_func=OrderCommentView.as_view('orders_comment'))
back.add_url_rule('/due_dates/<due_date_id>/',view_func=DueDateView.as_view('due_date'))
back.add_url_rule('/export/scholarships/',view_func=ScholarshipDownload.as_view('scholarship_download'))
back.add_url_rule('/export/attendance/',view_func=AttendanceDownload.as_view('attendance_download'))
back.add_url_rule('/daily_sheet/<kid_id>/send/',view_func=DailySheetMailView.as_view('daily_sheet_mail'))
back.add_url_rule('/weekly_hours/<kid_id>/',view_func=WeeklyHoursView.as_view('weekly_hours_view'))
back.add_url_rule('/weekly_hours/next/<kid_id>/',view_func=WeeklyHoursByDateView.as_view('weekly_hours_by_date_next_view'))
back.add_url_rule('/weekly_hours/prev/<kid_id>/',view_func=WeeklyHoursByDateView.as_view('weekly_hours_by_date_prev_view'))
back.add_url_rule('/token/',view_func=TokenRequestView.as_view('token_request_view'))