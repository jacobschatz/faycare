from flask import Blueprint, request, jsonify, g, abort, Response
from flask.views import MethodView
from faycare.models import *
from flask.ext.security import current_user, login_required, roles_required, roles_accepted, forms, auth_token_required
from flask_security.utils import verify_password
from itsdangerous import JSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
import json
import datetime
if inspect.stack()[-1][1] == 'mange.py':
	from test_server import app
elif inspect.stack()[-1][1] == '/usr/local/bin/gunicorn':
	from serve import app
else:
	from faycare.migrate_app import app

api = Blueprint('api', __name__, template_folder="templates", url_prefix='/api')

def get_late_fees(kid,dt):
	pass

def before_api():
	if request.method != 'POST':
		abort(401)
	data = request.json
	if not request.endpoint == 'api.loginapi' and not request.endpoint == 'api.checkapi':
		if 'email' in data and 'token' in data:
			s = Serializer(app.config['SECRET_KEY'])
			try:
				data = s.loads(data['token'])
				g.user = User.query.filter_by(email=data['email']).first()
				g.school = g.user.schools[0]
			except Exception, e:
				abort(401)
		else:
			abort(401)


api.before_request(before_api)

class ApiTokenView(MethodView):
	def get(self):
		if not current_user.is_anonymous():
			return jsonify({'token':current_user.get_auth_token(),'message':'OK'})
		else:
			return jsonify({'message':'NO GOOD'})

class ProtectedResourceView(MethodView):
	def post(self):
		print request.headers['Authorization']
		return jsonify({'message':'secret message'})

class LoginAPIView(MethodView):
	def post(self):
		data = request.json
		if 'username' in data and 'password' in data:
			user = User.query.filter_by(email=data['username']).first()
			if not user:
				return jsonify({'message':'Invalid email or password.','result':'NO GOOD'})
			else:
				if verify_password(data['password'],user.password):
					return jsonify({'message':'Found user and password ok','result':'OK',
						'token':user.get_mobile_access_token(),
						'user':user.id,
						'role':user.roles[0].name})
				else:
					return jsonify({'message':'Invalid email or password.','result':'NO GOOD'})
		return jsonify({'message':'Please supply a username and password'})

class CheckAPIView(MethodView):
	def post(self):
		data = request.json
		user = User.query.filter_by(email=data['email']).first()
		s = Serializer(app.config['SECRET_KEY'])
		try:
			unpacked = s.loads(data['token'])
			if unpacked['email'] == data['email']:
				return jsonify({'result':'OK',
					'message':'email and id matched',
					'user':user.id,
					'role':user.roles[0].name})
			else:
				return jsonify({'result':'NO GOOD','message':'no match'})
		except SignatureExpired:
			return jsonify({'result':'NO GOOD','message':'expired'})
		except BadSignature:
			return jsonify({'result':'NO GOOD','message':'bad sig'})
		
		return jsonify({'result':'OK'})

class KidsAPIView(MethodView):
	def post(self,kid_id=None):
		if kid_id:
			kid = Kid.query.get(kid_id)
			if not kid:
				return jsonify({result:'NO GOOD'})

			if(kid.school == g.school):
				return Response(json.dumps(kid.serialize),  mimetype='application/json')
			else:
				return jsonify({result:'NO GOOD'})
		else:
			return Response(json.dumps(g.school.serialize_kids), mimetype='application/json')

class ActivityAPIView(MethodView):
	def post(self):
		import pytz
		data = request.json
		if 'kid' in data:
			if 'activity' in request.json:
				kid = Kid.query.get(data['kid'])
				activity = Activity()
				activity.kid = kid
				activity.activity_type = ActivityType.query.get(data['activity'])
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				activity.time = dt
				activity.school = g.school
				activity.staff = g.user
				db.session.add(activity)
				db.session.commit()
				return json.dumps({'result':'OK'})
			else:
				kid = Kid.query.get(data['kid'])
				activities = kid.activities.order_by(Activity.id.desc()).limit(10)
				json_activities = []
				for activity in activities:
					json_activities.append(activity.serialize)

				return Response(json.dumps({'result':'OK','data':json_activities}), mimetype='application/json')
		else:
			json_activity_types = []
			for activity_type in ActivityType.query.all():
				json_activity_types.append(activity_type.serialize)
			return json.dumps(json_activity_types)

class BillsAPIView(MethodView):
	def post(self):
		data = request.json
		return Response(json.dumps({'result':'OKAY'}),mimetype='application/json')

class ActivityTypesAPIView(MethodView):
	def post(self):
		import pytz
		data = request.json
		if 'kid' in data and 'activity_type' in data:
			kid = Kid.query.filter(Kid.id == data['kid'],Kid.school==g.school).first()
			if not kid:
				return Response(json.dumps({'result':'NO GOOD'}), mimetype='application/json')

			activity = Activity()
			activity.kid = kid
			activity.activity_type = ActivityType.query.get(data['activity_type'])
			dt = datetime.datetime.utcnow()
			dt = dt.replace(tzinfo=pytz.utc)
			activity.time = dt
			activity.school = g.school
			activity.staff = g.user
			activity.description = ''
			db.session.add(activity)
			db.session.commit()

			return Response(json.dumps({'result':'OK'}), mimetype='application/json')
		else:
			return_data = []
			types = ActivityType.query.all()
			for activity_type in types:
				return_data.append(activity_type.serialize)

			return Response(json.dumps(return_data),mimetype='application/json')

class AttendanceCheckView(MethodView):
	def post(self):
		data = request.json
		if 'code' in data:
			user = User.query.filter(
				User.schools.contains(
					g.school
				)
			).filter(User.code==data['code']).first()
			if user:
				kid_obj = []
				for kid in user.kids:
					kid_obj.append({'id':kid.id,'name':'%s %s' % (kid.first_name,kid.last_name)})
				return Response(json.dumps({'result':'OK','data':kid_obj,'parent':user.id}),mimetype='application/json')
			else:
				return Response(json.dumps({'result':'NO GOOD'}),mimetype='application/json')

		if 'kids' in data:

			parent = User.query.get(data['parent'])
			for kid in data['kids']:
				kid = Kid.query.get(kid)
				status = int(data['status'])
				attendance = Attendance()
				dt = datetime.datetime.utcnow()
				dt = dt.replace(tzinfo=pytz.utc)
				attendance.time = dt
				attendance.school = g.school
				attendance.status = status
				attendance.kid = kid
				attendance.user = parent
				attendance.attendance_type = 0
				kid.status = status
				kid.status_time = dt
				get_late_fees(kid,dt)
				db.session.add(attendance)
				db.session.add(kid)
			db.session.commit()
			return Response(json.dumps({'result':'OK','quantity':len(data['kids']),'status':status}),mimetype='application/json')

		if 'kid' in data:
			json_attendances = []
			kid = Kid.query.get(data['kid'])
			attendance = Attendance.query.filter(Attendance.kid == kid).order_by(Attendance.time.desc()).limit(10)
			for att in attendance:
				json_attendances.append(att.serialize)
			return Response(json.dumps({'result':'OK','data':json_attendances}),mimetype='application/json')

		else:
			return json.dumps({'result':'NO GOOD'})




api.add_url_rule('/v1/token/',view_func=ApiTokenView.as_view('apitoken'))
api.add_url_rule('/v1/protected/',view_func=ProtectedResourceView.as_view('protectedresourceview'))
api.add_url_rule('/v1/login/',view_func=LoginAPIView.as_view('loginapi'))
api.add_url_rule('/v1/check/',view_func=CheckAPIView.as_view('checkapi'))
api.add_url_rule('/v1/kids/',view_func=KidsAPIView.as_view('kidsapi'))
api.add_url_rule('/v1/kids/<kid_id>/',view_func=KidsAPIView.as_view('kidsingleapi'))
api.add_url_rule('/v1/attendance/',view_func=AttendanceCheckView.as_view('attendance'))
api.add_url_rule('/v1/activities/',view_func=ActivityAPIView.as_view('activity'))
api.add_url_rule('/v1/activity_types/',view_func=ActivityTypesAPIView.as_view('activity_type'))
api.add_url_rule('/v1/bills/',view_func=BillsAPIView.as_view('billsapi'))