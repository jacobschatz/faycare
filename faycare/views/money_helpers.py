from flask import g
import datetime
from faycare.models import Credit, Bill, Price, db, CreditType, BetweenSchedule, Invoice, CalendarSchedule,Kid,School
from dateutil.relativedelta import relativedelta
from dateutil.rrule import MO,TU,WE,TH,FR,SA,SU,WEEKLY,rrule
import calendar

def days_of_week_open(kid,schedule=None,per_week=None,school=None):
	byweekday = []
	if not school:
		school = g.current_school
	# days till next billing cycle
	# If you didn't pass a schedule then get it
	if not schedule:
		schedule = get_schedule_for_kid(kid)

	# if you passed in the days of the week and this is not a calendar_schedule
	if per_week and not isinstance(schedule,CalendarSchedule):
		byweekday = []
		per_week = per_week.split(',')
		weekdays = {"MO":MO, "TU":TU, "WE":WE, "TH":TH, "FR":FR, "SA":SA, "SU":SU}
		for week in per_week:
			byweekday.append(weekdays.get(week))

	# otherwise this is prob a calendar schedule
	elif per_week and isinstance(schedule,CalendarSchedule):
		return {'next_cycle':per_week,'byweekday':byweekday}

	# otherwise damnit we'll figure it out on our own
	else:
		if isinstance(schedule,BetweenSchedule):
			if not schedule.closed_mon and not school.school_hours.closed_mon: byweekday.append(MO)
			if not schedule.closed_tues and not school.school_hours.closed_tues: byweekday.append(TU)
			if not schedule.closed_weds and not school.school_hours.closed_weds: byweekday.append(WE)
			if not schedule.closed_thurs and not school.school_hours.closed_thurs: byweekday.append(TH)
			if not schedule.closed_fri and not school.school_hours.closed_fri: byweekday.append(FR)
			if not schedule.closed_sat and not school.school_hours.closed_sat: byweekday.append(SA)
			if not schedule.closed_sun and not school.school_hours.closed_sun: byweekday.append(SU)
		else:
			# OK fine last straw let's just go by when the school is open
			if not school.school_hours.closed_mon: byweekday.append(MO)
			if not school.school_hours.closed_tues: byweekday.append(TU)
			if not school.school_hours.closed_weds: byweekday.append(WE)
			if not school.school_hours.closed_thurs: byweekday.append(TH)
			if not school.school_hours.closed_fri: byweekday.append(FR)
			if not school.school_hours.closed_sat: byweekday.append(SA)
			if not school.school_hours.closed_sun: byweekday.append(SU)

	return byweekday

def days_till_next_cycle(kid,schedule=None,per_week=None,school=None):
	if not school:
		school = g.current_school
	byweekday = days_of_week_open(kid,schedule,per_week,school)
	dt = datetime.datetime.utcnow() + relativedelta(months=1,day=school.cycle_start)
	next_cycle = rrule(WEEKLY, 
		byweekday=tuple(byweekday), 
		dtstart=datetime.datetime.utcnow(),
		until=dt).count()

	return {'next_cycle':next_cycle,'byweekday':byweekday}

def days_till_next_cycle_starting_next_month(kid,school,schedule=None,per_week=None):
	if not school:
		school = g.current_school
	byweekday = days_of_week_open(kid,schedule,per_week,school=school)
	dt_start = datetime.datetime.utcnow() + relativedelta(months=1,day=school.cycle_start)
	dt_end = datetime.datetime.utcnow() + relativedelta(months=2,day=school.cycle_start)
	next_cycle = rrule(WEEKLY, 
		byweekday=tuple(byweekday), 
		dtstart=dt_start,
		until=dt_end).count()

	return {'next_cycle':next_cycle,'byweekday':byweekday}

def prorate_and_bill(kid,force_billing=False):
	credit = attempt_prorate_kid(kid)
	if credit or force_billing:
		create_bill(kid,credit)

def attempt_prorate_kid(kid,school=None):
	if not school:
		school = g.current_school
	current_bill = kid.bills.filter_by(month=datetime.datetime.utcnow().month,year=datetime.datetime.utcnow().year).order_by(Bill.time.desc()).first()
	if not current_bill:
		return False

	cycle_start = school.cycle_start
	next_cycle_dict = days_till_next_cycle(kid,per_week=current_bill.days_per_week,school=school)
	next_cycle = next_cycle_dict.get('next_cycle')
	credit_amount = current_bill.per_day * next_cycle
	credit_type = CreditType.query.filter_by(name='prorate').first()
	credit = Credit(
		amount=credit_amount,
		time=datetime.datetime.utcnow(),
		description='Credit for %s applied to %s %s. %s school days unused.' % (u'{1}{0:.2f}'.format(credit_amount, '$'),kid.first_name,kid.last_name,next_cycle), 
		status=0,
		month=datetime.datetime.utcnow().month,
		year=datetime.datetime.utcnow().year,
		credit_type=credit_type,
		school=school,
		kid=kid)
	return credit

def get_schedule_for_kid(kid):
	schedule_types = ['one_for_all_schedule','hourly_schedule','calendar_schedule','between_schedule']
	for schedule in schedule_types:
		kid_schedule = getattr(kid,schedule)
		if kid_schedule: return kid_schedule

	return None

def bill_next_month_calendar(kid,credit=0):
	pass

def bill_next_month(kid_id,school_id,credit=0):
	now = datetime.datetime.utcnow()
	school = School.query.get(school_id)
	kid = Kid.query.get(kid_id)
	kid_schedule = get_schedule_for_kid(kid)
	if not kid_schedule:
		return None

	if isinstance(kid_schedule,CalendarSchedule):
		bill_next_month_calendar(kid,credit)
	price = kid_schedule.prices.filter(Price.group==kid.groups.first()).all()[0]
	next_cycle_dict = days_till_next_cycle_starting_next_month(kid,school,kid_schedule)
	next_cycle = next_cycle_dict.get('next_cycle')
	bill_description = '%i school days until the next billing cycle. New bill for %s %s.' % (next_cycle, kid.first_name, kid.last_name)

	per_day = 0
	if price.billing_unit == 0:
		# days
		per_day = price.price / price.billing_duration
		amount = per_day * next_cycle
	elif price.billing_unit == 1:
		# months
		days_in_this_month = calendar.monthrange(now.year,now.month)[1]
		per_month = price.price / price.billing_duration
		amount = 0
		if school.bill_equal:
			amount = per_month
		else:
			per_day = days_in_this_month / per_month
			amount = per_day * next_cycle

	next_month_date = datetime.date.today() + relativedelta(months=1)
	bill = Bill(time=now,
		description=bill_description,
		school=school,
		amount=amount,
		days_per_week=','.join([str(w) for w in next_cycle_dict.get('byweekday')]),
		per_day=per_day,
		month=next_month_date.month,
		is_calendar=False,
		year=next_month_date.year,
		status=0,
		kid=kid)
	invoice = Invoice.query.filter(Invoice.month==next_month_date.month,
		Invoice.year==next_month_date.year,
		Invoice.kid==kid).first()
	if not invoice:
		invoice = Invoice(month=next_month_date.month,
			year=next_month_date.year,
			time=now,
			kid=kid)
	if kid.scholarship > 0:
		credit_type = CreditType.query.filter_by(name='scholarship').first()
		#has scholarship this month already?
		scholarship_credit_next_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == next_month_date.month,
			Credit.year == next_month_date.year,
			Credit.credit_type == credit_type).count()
		if not scholarship_credit_next_month:
			scholarship_credit = Credit(
				amount=kid.scholarship,
				time=now,
				month=next_month_date.month,
				year=next_month_date.year,
				description='Credit applied for %s applied to %s %s.' % (u'{1}{0:.2f}'.format(kid.scholarship, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=school,
				kid=kid)
			db.session.add(scholarship_credit)
			bill.credits.append(scholarship_credit)
			invoice.credits.append(scholarship_credit)
	if kid.govt > 0:
		credit_type = CreditType.query.filter_by(name='government').first()
		govt_credit_next_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == next_month_date.month,
			Credit.year == next_month_date.year,
			Credit.credit_type == credit_type).count()
		if not govt_credit_next_month:
			govt_credit = Credit(
				amount=kid.govt,
				time=now,
				month=next_month_date.month,
				year=next_month_date.year,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.govt, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=school,
				kid=kid)
			db.session.add(govt_credit)
			bill.credits.append(govt_credit)
			invoice.credits.append(govt_credit)
	if kid.extra_credit > 0:
		credit_type = CreditType.query.filter_by(name='other').first()
		extra_credit_next_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == next_month_date.month,
			Credit.year == next_month_date.year,
			Credit.credit_type == credit_type).count()
		if not extra_credit_next_month:
			extra_credit = Credit(
				amount=kid.extra_credit,
				time=now,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.extra_credit, '$'),kid.first_name,kid.last_name), 
				status=0,
				month=next_month_date.month,
				year=next_month_date.year,
				credit_type=credit_type,
				school=school,
				kid=kid)
			db.session.add(extra_credit)
			bill.credits.append(extra_credit)
			invoice.credits.append(extra_credit)

	db.session.add(bill)
	invoice.bills.append(bill)

	db.session.add(invoice)
	db.session.commit()

def create_calendar_bill(kid,bill_dates,month,year):
	now = datetime.datetime.utcnow()
	if not bill_dates:
		return None

	price = g.current_school.calendar_schedule.prices.filter(Price.group==kid.groups.first()).first()
	bill_description = '%i calendar days billed. New bill for %s %s.' % (len(bill_dates), kid.first_name, kid.last_name)

	per_day = 0
	if price.billing_unit == 0:
		# days
		per_day = price.price / price.billing_duration
		amount = per_day * len(bill_dates)
	elif price.billing_unit == 1:
		# months
		per_month = price.price / price.billing_duration
		per_day = price.price / len(bill_dates)
		amount = per_month

	bill = Bill(time=now,
		description=bill_description,
		school=g.current_school,
		amount=amount,
		days_per_week=len(bill_dates),
		per_day=per_day,
		month=month,
		year=year,
		is_calendar=True,
		status=0,
		kid=kid)

	invoice = Invoice.query.filter(Invoice.month==month,
		Invoice.year==year,
		Invoice.kid==kid).first()
	if not invoice:
		invoice = Invoice(month=month,
			year=year,
			time=now,
			kid=kid)

	credit = attempt_prorate_kid(kid)
	if credit:
		bill.credits.append(credit)	

	if kid.scholarship > 0:
		credit_type = CreditType.query.filter_by(name='scholarship').first()
		#has scholarship this month already?
		scholarship_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not scholarship_credit_this_month:
			scholarship_credit = Credit(
				amount=kid.scholarship,
				time=now,
				month=now.month,
				year=now.year,
				description='Credit applied for %s applied to %s %s.' % (u'{1}{0:.2f}'.format(kid.scholarship, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(scholarship_credit)
			bill.credits.append(scholarship_credit)
			invoice.credits.append(scholarship_credit)
	if kid.govt > 0:
		credit_type = CreditType.query.filter_by(name='government').first()
		govt_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not govt_credit_this_month:
			govt_credit = Credit(
				amount=kid.govt,
				time=now,
				month=now.month,
				year=now.year,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.govt, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(govt_credit)
			bill.credits.append(govt_credit)
			invoice.credits.append(govt_credit)
	if kid.extra_credit > 0:
		credit_type = CreditType.query.filter_by(name='other').first()
		extra_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not extra_credit_this_month:
			extra_credit = Credit(
				amount=kid.extra_credit,
				time=now,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.extra_credit, '$'),kid.first_name,kid.last_name), 
				status=0,
				month=now.month,
				year=now.year,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(extra_credit)
			bill.credits.append(extra_credit)
			invoice.credits.append(extra_credit)

	db.session.add(bill)
	invoice.bills.append(bill)

	db.session.add(invoice)
	db.session.commit()

def create_bill(kid,credit=None):
	kid_schedule = get_schedule_for_kid(kid)
	now = datetime.datetime.utcnow()
	if not kid_schedule:
		return None

	price = kid_schedule.prices.filter(Price.group==kid.groups.first()).all()[0]
	next_cycle_dict = days_till_next_cycle(kid,kid_schedule)
	next_cycle = next_cycle_dict.get('next_cycle')
	bill_description = '%i school days until the next billing cycle. New bill for %s %s.' % (next_cycle, kid.first_name, kid.last_name)

	per_day = 0
	if price.billing_unit == 0:
		# days
		per_day = price.price / price.billing_duration
		amount = per_day * next_cycle
	elif price.billing_unit == 1:
		# months
		days_in_this_month = calendar.monthrange(now.year,now.month)[1]
		per_month = price.price / price.billing_duration
		amount = 0
		if g.current_school.bill_equal:
			amount = per_month
		else:
			per_day = days_in_this_month / per_month
			amount = per_day * next_cycle

	bill = Bill(time=now,
		description=bill_description,
		school=g.current_school,
		amount=amount,
		days_per_week=','.join([str(w) for w in next_cycle_dict.get('byweekday')]),
		per_day=per_day,
		month=now.month,
		is_calendar=False,
		year=now.year,
		status=0,
		kid=kid)
	invoice = Invoice.query.filter(Invoice.month==now.month,
		Invoice.year==now.year,
		Invoice.kid==kid).first()
	if not invoice:
		invoice = Invoice(month=now.month,
			year=now.year,
			time=now,
			kid=kid)
	if credit:
		bill.credits.append(credit)
		invoice.credits.append(credit)
	else:
		credit = attempt_prorate_kid(kid)
		if credit:
			bill.credits.append(credit)	
	if kid.scholarship > 0:
		credit_type = CreditType.query.filter_by(name='scholarship').first()
		#has scholarship this month already?
		scholarship_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not scholarship_credit_this_month:
			scholarship_credit = Credit(
				amount=kid.scholarship,
				time=now,
				month=now.month,
				year=now.year,
				description='Credit applied for %s applied to %s %s.' % (u'{1}{0:.2f}'.format(kid.scholarship, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(scholarship_credit)
			bill.credits.append(scholarship_credit)
			invoice.credits.append(scholarship_credit)
	if kid.govt > 0:
		credit_type = CreditType.query.filter_by(name='government').first()
		govt_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not govt_credit_this_month:
			govt_credit = Credit(
				amount=kid.govt,
				time=now,
				month=now.month,
				year=now.year,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.govt, '$'),kid.first_name,kid.last_name), 
				status=0,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(govt_credit)
			bill.credits.append(govt_credit)
			invoice.credits.append(govt_credit)
	if kid.extra_credit > 0:
		credit_type = CreditType.query.filter_by(name='other').first()
		extra_credit_this_month = Credit.query.filter(Credit.kid == kid,
			Credit.month == now.month,
			Credit.year == now.year,
			Credit.credit_type == credit_type).count()
		if not extra_credit_this_month:
			extra_credit = Credit(
				amount=kid.extra_credit,
				time=now,
				description='Credit applied for %s applied to %s %s..' % (u'{1}{0:.2f}'.format(kid.extra_credit, '$'),kid.first_name,kid.last_name), 
				status=0,
				month=now.month,
				year=now.year,
				credit_type=credit_type,
				school=g.current_school,
				kid=kid)
			db.session.add(extra_credit)
			bill.credits.append(extra_credit)
			invoice.credits.append(extra_credit)

	db.session.add(bill)
	invoice.bills.append(bill)

	db.session.add(invoice)
	db.session.commit()