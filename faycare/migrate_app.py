from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

app.config.from_object('faycare.config.ProductionConfig')
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://faycare:hatpants@localhost/faycare'
app.logger.info("Config: Production")