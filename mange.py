#set the path
import os, sys
from flask.ext.script import Manager, Server
import test_server
from flask.ext.migrate import MigrateCommand

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))


manager = Manager(test_server.app)

#Turn on debugger by default and reloader
manager.add_command("runserver", Server(
	use_debugger = True,
	use_reloader = True,
	host = '0.0.0.0')
)

manager.add_command('db', MigrateCommand)

if __name__ == "__main__":
	manager.run()